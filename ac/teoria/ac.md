---
title: Arquitectura de Computadores
author: delightfulagony
lang: es-ES
documentclass: article
papersize: a4
pagestyle: headings
margin-left: 20mm
margin-right: 20mm
margin-top: 30mm
---
# Tema 1: Arquitecturas paralelas: Clasificación y prestaciones
## Arquitecturas paralelas: Clasificación y prestaciones
El uso del paralelismo en computadores permite aumentar las 
prestaciones del sistema. Podemos implementar el paralelismo
aprovechando las entradas de dos formas:

- Replicando componentes del sistema.
- Segmentando el uso de esos componentes.

#### Dependencias de datos

Una dependencia de datos de $B_2$ respecto a $B_2$ debe cumplir 
2 condiciones:

- Ambos bloques deben referenciar una misma posición en memoria.
- $B_1$ debe aparecer antes que $B_2$ en el código.

##### Tipos de dependencias
- **RAW** Read After Write *(Dependencia verdadera)* `a=b+c; d=a;`
- **WAR** Write After Read *(Anti-dependencia)* `a=b+c; b=d;`
- **WAW** Write After Write *(Dependencia de salida)* `a=c; a=d;`

## Tipos de paralelismo implícito

### Paralelismo funcional

##### Niveles de paralelismo implícito
Podemos dividir un programa en distintos niveles, de mayor a menor
profundidad:

- **Nivel de programa.** Generalmente los diferentes programas que intervienen en una
aplicación pueden ejecutarse en paralelo pues es poco probable que haya dependencias
entre ellos.
- **Nivel de funciones.** Un programa está constituido por funciones, éstas podrán
ejecutarse en paralelo si no hay dependencias inevitables entre ellas *(RAW)*.
- **Nivel de bucle.** Una función puede estar a su vez compuesta por la ejecución de uno
o varios bucles. Se pueden ejecutar en paralelo las distintas iteraciones de un bucle
siempre que resolvamos las dependencias verdaderas.
- **Nivel de operaciones.** Podremos ejecutar en paralelo aquellas operaciones que sean
independientes entre sí. En los procesadores podremos encontrar instrucciones compuestas
por varias operaciones secuenciales sobre el mismo flujo de entrada, lo que podremos usar
para paralelizar evitando penalizaciones por dependencias de datos.

Este paralelismo que es posible detectar en distintos niveles del código lo llamaremos
**paralelismo funcional**.

### Paralelismo de tareas
El paralelismo de tareas se extrae mediante la división de la aplicación en tareas
independientes. En esta estructura cada bloque consiste en una función y las conexiones
entre estos bloques reflejan el flujo de datos entre las funciones. Este tipo de
paralelismo está muy relacionado con el paralelismo a nivel de función.

### Paralelismo de datos
El paralelismo de datos está muy relacionado con el paralelismo a nivel de bucle.
Está implícito en operaciones con estructuras de datos como vectores y matrices.
Estas estructuras se operan mediante bucles, así podemos paralelizar las operaciones 
realizadas sobre la misma estructura de datos pero en distintas iteraciones de éstas.

---

Podemos también clasificar el paralelismo en función de la granuralidad de la tarea.
Llamaremos **grano fino** al paralelismo entre operaciones o instrucciones, **grano 
medio** al paralelismo entre bloques funcionales lógicos de la aplicación y **grano
grueso** al paralelismo entre programas.

### Unidades de ejecución
El sistema operativo se encarga de gestionar la ejecución de las unidades de mayor
granuralidad como son la hebras y los procesos. Cada una de estas unidades tiene su
propia región en memoria, además, los SOs multihebra permiten descomponer un proceso
en una o más hebras.

La **diferencia entre un proceso y una hebra** reside en que una hebra tiene su propia
pila y registros pero comparte código, variables globales y otros recursos como archivos
abiertos con el resto de hebras del proceso. Gracias a esto las hebras se pueden crear y
destruir más rápido que los procesos y la comunicación, sincronización y conmutación es 
más rápida que entre procesos.

Por tanto las hebras tienen una **menor granuralidad** que los procesos.
El paralelismo implícito en una aplicación puede hacerse explícito a nivel de instrucciones,
hebras, procesos o dentro de una instrucción.

### Paralelismo explícito, implícito y arquitecturas paralelas
Tenemos distintas formas de hacer explícito el paralelismo:
- El paralelismo a **nivel de programa** se realiza por medio de procesos. Al ejecutarse un
programa se crea su proceso asociado.
- El paralelismo a **nivel de funciones** puede realizarse por medio de procesos o de hebras.
- El paralelismo a **nivel de bucles** puede realizarse por medio de procesos o hebras.
Podremos además aumentar la granuralidad aumentando el número de iteraciones que realiza cada
unidad de ejecución paralela. Las instrucciones vectoriales también hacen explícito este
paralelismo.
- El paralelismo a **nivel de operaciones** puede realizarse en arquitecturas con paralelismo
a nivel de instrucción.

![Tipos de paralelismo explícito](./_assets/tipos_paralelismo.png){width=75%}

### Detección, utilización, implementación y extracción del paralelismo.
El grado de paralelismo de un conjunto de entradas a un sistema es el máximo número de
entradas del conjunto que se pueden ejecutar en paralelo. En los procesadores las entradas
son las instrucciones.

![Utilización, implementación y extracción del paralelismo](./_assets/implementacion_paralelismo.png){width=90%}

### Clasificación de arquitecturas paralelas
#### Clasificación de arquitecturas y sistemas paralelos
El paralelismo puede implementarse en las arquitecturas siguiendo dos lineas fundamentales:

- **La replicación de elementos** consiste en replicar los componentes del sistema para
aumentar sus prestaciones, se replican unidades funcionales, procesadores, módulos de memoria,
etc. y se distribuye el trabajo entre ellas.
- **La segmentación de cauces** consiste en dividir o *segmentar* alguno de los componentes
del sistema en etapas independientes, de forma que pueden realizarse simultáneamente distintas
etapas del funcionamiento de éste.

#### Clasificación de Flynn de arquitecturas
La clasificación de Flynn clasifica los computadores de acuerdo a si es capaz de gestionar
flujos de datos o de instrucciones de forma simultánea:

- **SISD:** *Single Instruction-Single Data*
- **SIMD:** *Single Instruction-Multiple Data*
- **MIMD:** *Multiple Instruction-Multiple Data*
- **MISD:** *Multiple Instruction-Single Data* (Estos computadores no existen de manera
explícita pues pueden implementarse sobre un computador MIMD)

#### Clasificación según el sistema de memoria

- **Memoria compartida** *(Shared Memory, SM)* también llamados **multiprocesadores**. En 
estos sistemas los procesadores comparten el espacio de direcciones y el programador no
necesita saber donde se almacenan los datos.
- **Memoria distribuida** *(Distributed Memory, DM)* también llamados **multicomputadores**.
En estos sistemas cada procesador tiene su propio sistema de direcciones por lo que el 
programador necesita saber donde se almacenan los datos.

#### SMP frente a Multiprocesadores

| Symetric Multi-Processor | Multicomputador |
| :---: | :---: |
| Latencia alta | Latencia baja |
| Poco escalable | Más escalable |
| Comunicación mediante variables compartidas, datos no duplicados |\
Comunicación mediante software de paso de mensajes, datos duplicados |
| Necesita implementar primitivas de sincronización |\
Sincronización mediante software de comunicación |
| No necesita distribuir el código y los datos entre varios procesadores |\
Requiere distribuir código y datos entre distintos procesadores
| Programación más sencilla | Programación más complicada |

#### Incremento de escalabilidad en multiprocesadores y red de conexión
Podremos realizarlo de 3 formas:

- Aumentando la caché del procesador
- Usando redes de menor latencia y mayor ancho de banda que un bus, podremos
hacerlo mediante **una jerarquía de buses**, mediante **redes multietapa** o 
mediante una red de **barras cruzadas**.

Jerarquía de buses | Multietapa | Barras cruzadas
--- | --- | ---
![Jerarquía de buses](./_assets/jerarquia_buses.jpg){width=50%} | ![Multietapa](./_assets/multietapa.png){width=20%} | ![Barras cruzadas](./_assets/barras_cruzadas.png){width=20%}

![Clasificacion completa de computadores según el sistema de memoria](./_assets/clasificacion_memoria.png){width=50%}

#### Propuesta de clasificación de computadores con múltiples threads
- **TLP** *(Thread-level parallelism)* Múltiples flujos de control en paralelo.
	- **Implícito:** Gestionado por la arquitectura.
	- **Explícito:** Gestionado por el SO.
		- **Con una instancia SO:** multiprocesadores, multicores, multithread.
		- **Con múltiples instancias SO:** multicomputadores.

## Evaluación de prestaciones de una arquitectura
### Medidas usuales para evaluar prestaciones
#### Tiempo de CPU
$$
T_{CPU}=NI\times CPI\times T_{Ciclo}
$$
Hay procesadores que pueden emitir **múltiples instrucciones simultáneas**.
Podremos calcular el $CPI$ de la siguiente manera:
$$
CPI = \frac{CPE}{IPE}
$$
Donde $CPE$ representa los ciclos por emisión e $IPE$ representa el número de 
instrucciones por emisión.

Hay procesadores que pueden **codificar varias operaciones en una instrucción**.
Podremos calcular el $NI$ de la siguiente manera:
$$
NI = \frac{N_{Operaciones}}{Operaciones_{Por Instrucción}}
$$

#### Medidas de productividad
##### MIPS: Millions of Instructions Per Second
$$
MIPS = \frac{NI}{T_{CPU}\times 10^6} = \frac{F}{CPI\times 10^6}
$$

##### MFLOPS: Millions of FLoating point Operations Per Second
$$
MFLOPS = \frac{Operaciones_{ComaFlotante}}{T_{CPU}\times 10^6}
$$

#### Ganancia en prestaciones
Podemos expresar la ganancia en prestaciones en una máquina al aumentar
alguno de sus recursos mediante:
$$
S_p = \frac{V_p}{V_1} = \frac{T_1}{T_p}
$$

- $V_1$ Velocidad inicial
- $V_p$ Velocidad tras la mejora
- $T_1$ Tiempo de ejecución inicial
- $T_p$ Tiempo de ejecución tras la mejora

#### Ley de Amdahl
La **ley de Amdahl** establece que la mejora de velocidad que se puede obtener
cuando se mejora un recurso de una máquina en un factor p está limitada por:
$$
S \leq \frac{p}{1+f(p-1)} = \frac{T_Secuencial}{T_Paralelo}
$$
siendo f la fracción del tiempo de ejecución en la máquina inicial donde no puede
aplicarse la mejora.

\pagebreak

# Tema 2: Programación Paralela
La programación paralela plantea una serie de nuevos problemas con respecto a la
programación secuencial: la división tareas independientes, la asignación de tareas
o carga de trabajo en procesos/threads, la asignación de tareas a procesadores/núcleos 
y la sincronización y comunicación de éstas.

Estos problemas deberán abordarlos la herramienta de programación paralela, el
programador o el SO.

## Modos de programación paralela
- **SPMD** *(Single Program-Multiple Data)* El código es común y cada procesador
trabaja con un conjunto de datos diferente.
- **MPMD** *(Multiple Program-Multiple Data)* La aplicación principal se divide
en programas con códigos distintos, cada programa se ejecuta en un procesador con
un conjunto de datos diferente.

## Herramientas para obtener código paralelo
Las herramientas para obtener programas paralelos deben permitir de forma explícita
(el trabajo lo hace el programador) o implícita (el trabajo lo hace la propia
herramienta) las siguientes tareas:

- Localizar paralelismo o dividir en tareas
- Asignar las tareas
- Crear y terminar procesos/threads
- Comunicar y sincronizar procesos/threads
- Asignar procesos/threads a uds. de procesamiento

## Comunicaciones colectivas
![Comunicación uno a todos](./_assets/uno_a_todos.png){width=75%}

![Comunicación todos a uno](./_assets/todos_a_uno.png){width=75%}

![Comunicación múltiple uno a uno](./_assets/multiple_uno_a_uno.png){width=75%}

![Comunicación todos a todos](./_assets/todos_a_todos.png){width=75%}

\pagebreak

## Comunicaciones colectivas compuestas

![Todos combinan](./_assets/todos_combinan.png){width=35%}

![Recorrido *(scan)*](./_assets/recorrido.png){width=75%}

\pagebreak

## Estilos de programación paralela
### Paso de mensajes
Se implementa por medio de dos funciones:

- **send(destino,datos)** Permite enviar datos entre procesos/threads
- **receive(fuente,datos)** Permite recibir datos de otros procesos/threads

Puede implementarse con un paso de mensajes **síncrono** *(send bloquea el proceso
hasta que el destino lo receive y viceversa con receive)* o **asíncrono** *(send
no bloquea, receive sí)*. **MPI** es una API que realiza esta implementación.

### Variables compartidas
Se implementa usando variables compartidas entre los procesos/threads, las hebras
de un proceso comparten variables globales inmediatamente, pero para hacerlo entre
procesos habremos de usar llamadas al sistema. **OpenMP** realiza esta implementación.

### Paralelismo de datos
Este estilo aprovecha el paralelismo de datos inherente a las estructuras de datos como
vectores o matrices. El programador se encarga de escribir el código de forma que se
aproveche ese paralelismo y debido a estas estructuras las sincronizaciones son implícitas.

## Estructuras típicas de códigos paralelos
### Master-Slave *(granja de tareas)*
Puede implementarse de forma mixta usando **SPMD** para el *master* y **MPMD** para
los *slaves* o mediante **SPMD** con un solo programa para ambos.

Es también la estructura utilizada en el diseño de **cliente-servidor**.

[comment]: # Revisar la explicación de la forma mixta

![Master-Slave](./_assets/master-slave.png){width=60%}

### Descomposición de datos
Se usa en grandes estructuras de datos, consiste en descomponer estas estructuras
en estructuras más pequeñas sobre las que se realizarán tareas paralelas.

![Descomposición de datos](./_assets/descomposicion-datos.png){width=60%}

### Divide y vencerás
Es una estrategia consistente en dividir recursivamente un problema en problemas
más pequeños e independientes, de forma que puedan realizarse en paralelo y luego
combinar los resultados para obtener la solución del problema inicial.

![Divide y vencerás](./_assets/divide_y_venceras.png){width=60%}

## Proceso de paralelización

- Descomponer en tareas independientes
- Asignar tareas a procesos/threads
- Redactar código paralelo
- Evaluar prestaciones

### Descomposición en tareas independientes
Podemos extraer paralelismo a distintos niveles:

- **Nivel de funciones** *(Paralelismo de tareas)*
- **Nivel de bucle** *(Paralelismo de datos)*

### Asignación de tareas
No es recomendable asignar más de un proceso o hebra por procesador. Si queremos
utilizar procesos/threads influyen varios factores:

- La arquitectura del procesador. En SMPs y multihebras será recomendable el uso de
hebras mientras que en arquitecturas mixtas podremos usar tanto hebras como procesos.
- El SO deberá ser multihebra.
- La herramienta de programación debe permitir usar hebras.

Generalmente se asignan a hebras las iteraciones de un bucle y las funciones a procesos.
La arquitectura puede ser **heterogénea** *(componentes con distintas prestaciones)* u 
**homogénea** *(todos los componentes tienen las mismas prestaciones)*. Las
arquitecturas homogéneas son **uniformes** la comunicación entre componentes es siempre 
la misma.

Cuando las arquitecturas son homogéneas pero **no uniformes** se dificulta la
asignación de tareas.

La asignación de tareas puede realizarse de forma **estática** *(en tiempo de
compilación)* o de forma **dinámica** *(en tiempo de ejecución)*.

### Redactar código paralelo
Habrá de decidirse el estilo de programación y el modo de programación de éste y 
se procederá a escribir el código de acuerdo con esos paradigmas.

## Evaluación de prestaciones en procesamiento paralelo
En computadores paralelos se usan principalmente dos medidas de prestaciones:

- Tiempo de ejecución
- Productividad *(Numero de entradas que el computador es capaz de procesar por
unidad de tiempo)*

### Ganancia en prestaciones. Escalabilidad
La **ganancia en velocidad** permite estudiar de que forma se incrementan las
prestaciones al aumentar el número de procesadores.
$$
S(p) = \frac{T_{Secuencia}}{T_{Paralelo}}
$$
$T_{Paralelo} = T_{Cómputo} + T_{Overhead}$ donde $T_{Overhead}$ es la penalización

![Escalabilidad](./_assets/escalabilidad.png){width=35%}

Se dan varios casos:

- **Ganancia lineal:** El grado de paralelismo ha de ser ilimitado y el *overhead* despreciable.
$S_P=\frac{T_Secuencial}{T_paralelo}$
- **Ganancia superlineal:** Al aumentar el número de procesadores aumentamos también el de
otros recursos. $S_P>p$
- **Limitación al aprovechar el grado de paralelismo:** Se produce cuando hay código no
paralelizable y no pueden mejorarse las prestaciones por medio de paralelismo.
- **Reducción debido a la sobrecarga:** El incremento del número de procesadores no mejora
las prestaciones pero aumenta el *overhead*.

### Ley de Amdahl
La mejora de prestaciones está limitada por la fracción de código no paralelizable.
$$ S(p) \leq \frac{p}{1+f(p-1)} $$

- $S:$ Incremento de velocidad.
- $p:$ Incremento máximo que se puede conseguir si todo el código es paralelizable, *(numero de
procesadores)*.
- $f:$ fracción de tiempo en la que no se utiliza la mejora.

### Ley de Gustafson
Una vez llegado a un tiempo de ejecución paralela aceptable, podemos mantener el tiempo
constante aumentando el tamaño del problema si aumentamos el número de procesadores.
$$ S(p)=p(1-f)+f $$

La ley de Amdahl supone que $T_Secuencial$ es constante mientras que la de Gustafson supone
que $T_Paralelo$ es constante.

### Eficiencia
La eficiencia mide si las prestaciones reales se corresponden con las prestaciones teóricas
que debería tener.
$$ E(p,n)=\frac{Prestaciones(p,n)}{Prestaciones(1,n)}=\frac{S(p,n)}{p} $$

- $p$ representa el numero de procesadores.
- $n$ representa el tamaño del problema.

\pagebreak

# Tema 3: Arquitecturas con paralelismo a nivel de thread
## Arquitecturas TLP
### Clasificación de arquitecturas con TLP explícito y una instancia de SO

- Los **multiprocesadores** ejecutan varios threads en paralelo en un computador.
- Los **multicores** *(CMP)* ejecutan varios threads en paralelo en un chip multicore.
- Los **core multithread** son cores cuya arquitectura ILP se modifica para ejecutar threads
en paralelo.

### Cores multithread
Se componen de varias etapas:

- ***I**nstruction **F**etch*: Captación.
- ***I**nstruction **D**ecode*: Decodificación y emisión.
- ***Ex**ecution y **Mem**ory*: Ejecución y acceso a memoria.
- ***W**rite **B**ack*: Almacenamiento de resultados.

Los procesadores **segmentados** ejecutan instrucciones concurrentemente segmentando el uso
de sus componentes.

Los procesadores **VLIW** y **superescalares** ejecutan instrucciones **concurrentemente**
*(segmentación)* y **en paralelo** *(tienen múltiples unidades funcionales)*.

- VLIW:
	- Las instrucciones que se ejecutan en paralelo se captan juntas en memoria.
	- Ese conjunto de instrucciones conforma la palabra de instrucción muy larga.
	- El hardware presupone que las instrucciones de la palabra son independientes.
- Superescalares:
	- Tiene que encontrar instrucciones independientes por medio de hardware.

### Clasificación de cores multithread

- **Temporal MultiThreading** *(TMT)*
	- Ejecutan varios threads concurrentemente en el mismo core.
	- La conmutación entre threads la gestiona el hardware.
	- Emite instrucciones de un único thread en un ciclo.
- **Simultaneous MultiThreading** *(SMT)* o **multihilo**
	- Ejecutan en un core superescalar varios threads en paralelo.
	- Pueden emitir instrucciones de varios threads en un ciclo.

#### Clasificación de cores TMT
- **Fine-Grain MultiThreading** *(FGMT)*
	- La conmutación entre threads la decide el hardware cada ciclo por lo que tiene coste 0.
	- Usa algoritmos de planificación.
- **Coarse-Grain MultiThreading** *(CGMT)*
	- La conmutación entre threads la decide el hardware (coste 0 a varios ciclos).

##### Clasificación de cores CGMT con conmutación por eventos
- **Estática:** La conmutación puede ser explícita *(instrucciones añadidas al repertorio)* o 
implícita *(instrucciones existentes)*. Su ventaja es que el coste de cambio de contexto es
muy bajo, pero realiza cambios de contexto innecesarios.
- **Dinámica:** La conmutación puede realizarse por un fallo en la última caché, una
interrupción, etc. Su ventaja es que reduce los cambios de contexto innecesarios, pero el coste
del cambio de contexto es mayor.

![Core escalar segmentado](./_assets/escalar-segmentado.png){width=60%}

![Core superescalar](./_assets/superescalar.png){width=60%}

![Multicore](./_assets/multicore.png){width=60%}

\pagebreak

### Hardware y arquitecturas TLP 

![Hardware y arquitecturas TLP en un chip](./_assets/hardware_y_arquitecturas_TLP_en_un_chip.png){width=60%}

## Coherencia del sistema de memoria
### Sistema de memoria en multiprocesadores
El uso de jerarquía de memoria permite que haya varias copias de un mismo bloque
en memoria. El *sistema de memoria* incluye cachés de todos los nodos, memoria principal,
controladores, buffers, y una red de interconexion.

Para que el sistema de memoria sea coherente, una operación de lectura debe devolver lo
último que se ha escrito.

### Concepto de coherencia en el sistema de memoria: situaciones de incoherencia y requisitos
para evitar problemas en estos casos
Una **incoherencia de memoria** puede darse cuando en las distitnas copias de una dirección
no hay el mismo contenido.

### Métodos de actualizacicón de memoria principal implementados en caches

- **Escritura inmmediata:** *(write-through)* Cada vez que un procesador escribe
en su cache escribe también en memoria principal.
- **Posescritura:** *(write-back)* Se actualiza memoria principal escribiendo
todo el bloque cuando se desaloja de la cache.

En sistemas con múltiples caches puede haber faltas de coherencia entre caches. Los
**protocolos de coherencia de cache** resuelven este problema haciendo que cada 
escritura sea visible a todos los procesadores.

#### Alternativas para propagar una escritura en protocolos de coherencia de cache

- **Escritura con actualización:** *(write-update)* Cada vez que se escribe en una caché, se actualizan
el resto de copias de esa caché.
- **Escritura con invalidacion** *(write-invalidate)* Cada vez que se escribe en una caché se invalidan
el resto de copias de esa caché, tratar de leer una caché inválida provoca un fallo de caché, lo que
actualiza la copia.

### Requisitos del sistema de memoria para evitar problemas por incoherencia
- **Propagar las escrituras en una dirección**, deben hacerse visibles en un tiempo finito a otros
procesadores.
- **Serializar las escrituras en una dirección**, deben verse en el mismo orden por todos los
procesadores.

