#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define WSIZE 8*sizeof(unsigned)
#define SIZE 1<<20

unsigned lista[SIZE];
int resultado=0;

/**
 * @brief Calcula la paridad con un bucle for.
 */
int parity_1(unsigned* array, int len) {
    int  i, j, res=0, val=0;
    for (i=0; i<len; i++) {
	for (j = 0, val=0 ; j < WSIZE; j++) {
		unsigned mask=0x1 << j;
		val^= (array[i] & mask) != 0;
	}
	res+=val;
    }
    return res;
}

/**
 * @brief Calcula la paridad con un bucle while
 */
int parity_2(unsigned* array, int len) {
    int  i, j, res=0, val=0;
    for (i=0; i<len; i++) {
	j=0;
	while(j < WSIZE) {
		unsigned mask=0x1 << j;
		val^= (array[i] & mask) != 0;
		j++;
	}
	res+=val;
    }
    return res;
}

/**
 * @brief Calcula la paridad bit a bit.
 */
int parity_3(unsigned* array, int len) {
	int i;
	unsigned res=0, val=0, mask=0x1, x;
	for(i=0; i<len; i++) {
		x=array[i];
		while(x) {
			val^=x;
			x>>=1;
		}
		res+=(val & mask);
		val=0;
	}
	return res;
}

/**
 * @brief Calcula la paridad con ensamblador
 */
int parity_4(unsigned* array, int len) {
	int i;
	unsigned res=0, val=0, mask=0x1, x;
	for(i=0; i<len; i++) {
		x=array[i];
		val=0;
		asm("\n"
		".ini3:		\n\t"
		"xor %[x], %[v]	\n\t"
		"shr %[x]	\n\t"
		"jnz .ini3	\n\t"
		"and $0x1, %[v]	\n"
		: [v] "+r" (val)
		: [x] "r" (x)
		);
		res+=val;
	}
	return res;
}

/**
 * @brief Calcula la paridad por comparaciones
 */
int parity_5(unsigned* array, int len) {
	int i, j;
	unsigned res=0, mask=0x1, x;
	for(i=0; i<len; i++) {
		x=array[i];
		for(j=16; j>=1; j/=2)
			x^=x>>j;
		res+=(x & mask);
	}
	return res;
}

/**
 * @brief Calcula la paridad por comparaciones en ensamblador
 */
int parity_6(unsigned* array, int len) {
	int i, j;
	unsigned res=0, mask=0x1, x;
	for(i=0; i<len; i++) {
		x=array[i];
		asm("\n"
		"mov	%[x], %%edx	\n\t"
		"shr	$0x10, %[x]	\n\t"
		"xor	%[x], %%edx	\n\t"
		"shr	$0x08, %[x]	\n\t"
		"xor    %[x], %%edx	\n\t"
		"setpo  %%dl		\n\t"
		"movzx %%dl, %[x]	\n"
		: [x] "+r" (x)
		:
		: "edx"
		);
		res+=x;
	}
	return res;
}

void crono(int (*func)(), char* msg) {
    struct timeval tv1,tv2;
    long           tv_usecs;

    gettimeofday(&tv1,NULL);
    resultado = func(lista, SIZE);
    gettimeofday(&tv2,NULL);

    tv_usecs=(tv2.tv_sec -tv1.tv_sec )*1E6+
             (tv2.tv_usec-tv1.tv_usec);
    printf("resultado = %d\t", resultado);
    printf("%s%9ld\n", msg, tv_usecs);
}

int main() {
	int i;
	for (i=0; i<SIZE; i++)
		lista[i]=i;

	crono(parity_1,"(1ª versión)");
	crono(parity_2,"(2ª versión)");
	crono(parity_3,"(3ª versión)");
	crono(parity_4,"(4ª versión)");
	crono(parity_5,"(5ª versión)");
	crono(parity_6,"(6ª versión)");
	return 0;
}
