.section .data
.macro linea
	.int -1000000000,-1000000000,1000000000,1000000000
.endm
lista: 
	.irpc i, 12345678
		linea
	.endr
longlista:	.int (.-lista)/4
resultado:	.int -1

.section .text
main: .global main

	mov 	$lista, %ebx		# %ecx será el puntero a lista
	mov 	longlista, %ecx		# %esi contendrá el tamaño de lista
	call	suma			# goto l28

	idiv	%ecx			# 'idiv' realiza la división de
					# %edx:%eax/%esi

	mov	%eax, resultado		# Movemos %eax a resultado

	mov	$1, %eax		# Ponemos los flags de estado y
	mov 	$0, %ebx		# hacemos una llamada a sistema
	int 	$0x80			# para finalizar el programa

suma:
	mov	$0, %eax		# Inicializamos todos los registros
	mov 	$0, %edx		# que vamos a necesitar
	push 	%esi
	mov 	$0, %esi
	push 	%edi
	mov	$0, %edi
	push 	%ebp			# Guardamos %ebp para recuperarlo
	mov 	$0, %ebp		# luego ya que es un registro
					# relativamente importante y sin el
					# puede que no podamos returnear
bucle:
	mov 	(%ebx,%esi,4), %eax	# Movemos el siguiente entero de la
					# lista a %eax
	cdq				# Vemos el signo de %eax

	add	%eax, %ebp		# Sumamos %eax al registro que hemos
					# elegido como acumulador del bucle
	adc 	%edx, %edi		# Sumamos el acarreo de 'add' junto con
					# el signo obtenido con 'cdq' para el 
					# acarreo con signo
	inc	%esi			# Sumamos 1 al contador del bucle
	cmp	%esi,%ecx		# Comparamos el contador con el tamaño
					# de la lista que esta en %esi (l19)
	jne	bucle			# Si no son iguales goto 'bucle' (l37)

	mov 	%ebp, %eax		# Movemos los valores resultado a %eax
	mov 	%edi, %edx		# y %edx siendo %eax el menos
					# significativo (Little-Endian)

	pop 	%ebp			# Devolvemos el valor original a %ebp
	pop 	%edi
	pop 	%esi

	ret
