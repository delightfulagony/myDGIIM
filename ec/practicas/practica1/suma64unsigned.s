.section .data
lista:		
	.int 	0x08000000,0x08000000,0x08000000,0x08000000
	.int 	0x08000000,0x08000000,0x08000000,0x08000000
	.int 	0x08000000,0x08000000,0x08000000,0x08000000
	.int 	0x08000000,0x08000000,0x08000000,0x08000000
	.int 	0x08000000,0x08000000,0x08000000,0x08000000
	.int 	0x08000000,0x08000000,0x08000000,0x08000000
	.int 	0x08000000,0x08000000,0x08000000,0x08000000
	.int 	0x08000000,0x08000000,0x08000000,0x08000000
longlista:	.int (.-lista)/4
resultado:	.quad -1

.section .text
main: .global main


	mov 	$lista, %ebx		# %ebx será el puntero a lista
	mov 	longlista, %ecx		# %ecx contendrá el tamaño de lista
	call	suma			# goto l28
	mov	%eax, resultado		# Movemos %eax a resultado y
	mov 	%edx, resultado+4	# %edx a resultado+4 para crear un
					# entero de 64bits en Little-Endian
	mov	$1, %eax		# Ponemos los flags de estado y
	mov 	$0, %ebx		# hacemos una llamada a sistema
	int 	$0x80			# para finalizar el programa

suma:
	mov	 $0, %eax		# Inicializamos todos los registros
	mov 	 $0, %edx		# que vamos a necesitar
	mov	 $0, %esi
bucle:
	add	 (%ebx,%esi,4), %eax	# Sumamos a %eax el siguiente valor 
					# de la lista de valores
	adc 	 $0,  %edx		# Sumamos el acarreo a %edx
	inc	 %esi			# Añadimos 1 al iterador del bucle
	cmp	 %esi,%ecx		# Comparamos el iterador con la
					# longitud de la lista
	jne	 bucle			# Si %esi y %eci son iguales seguimos
					# adelante
	ret				# Salimos de la funcion a l20
