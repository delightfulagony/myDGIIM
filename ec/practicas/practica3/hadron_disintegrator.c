#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
//gcc -m32 hadron_disintegrator.c -o hadron_disintegrator.bomb

char passphrase[]="h1gh_t3ch";
int passcode=842610;

void bomb() {
	printf("       _______\n");
        printf("      /  /  / ''..\n");
        printf("    |~||~||~|     '.\n");
        printf("    | || || |      : (~)T(~)\n");
        printf("    | || || |      '. /   \\ \n");
        printf("    | || || |         \\ | /\n");
        printf("    | || || |         /^^^\\\n");
        printf("    |_||_||_| 	\n");
}

void success() {
	printf("       _______\n");
        printf("      /  /  / ''..\n");
        printf("    |~||~||~|       \n");
        printf("    | || || |        (~)T(~)\n");
        printf("    | || || |      __ /   \\ \n");
        printf("    | || || |     :   \\ | /\n");
        printf("    | || || |    :    /^^^\\\n");
        printf("    |_||_||_|    *\n");
	exit(0);
}

void fail() {
	printf("                            ____\n");
	printf("                    __,-~~/~    `---.\n");
	printf("                  _/_,---(      ,    )\n");
	printf("              __ /        <    /   )  \\___\n");
	printf("- ------===;;;'====------------------===;;;===----- -  -\n");
	printf("                  \\/  ~\"~\"~\"~\"~\"~\\~\"~)~\"/\n");
	printf("                 (_ (   \\  (     >    \\)\n");
	printf("                  \\_( _ <         >_>'\n");
	printf("                     ~ `-i' ::>|--\"\n");
	printf("                          I;|.|.|\n");
	printf("                         <|i::|i|`.\n");
	printf("                        (` ^'\"`-' \")\n");
	printf("------------------------------------------------------------------\n");
	exit(-1);
}

int main() {
	char tryphrase[100];
	struct timeval tv1,tv2;
	
	bomb();
	printf("Dispones de 1 minuto para desactivar la bomba.\n");
	printf("El tiempo comienza ya.\n");
	gettimeofday(&tv1,NULL); 
	printf("Buena suerte\n");
	
	printf("Passphrase: ");
	fgets(tryphrase,100,stdin);
	if(strncmp(tryphrase,passphrase,strlen(passphrase))) fail();

	int trycode;
	gettimeofday(&tv2,NULL); 
	while(tv2.tv_sec - tv1.tv_sec < 60) {
		printf("Introduzca el código de confirmación: ");
		scanf("%i",&trycode);
		if(trycode==passcode) success();
		gettimeofday(&tv2,NULL); 
		printf("Error!\n");
	}
	fail();

	return 0;
	}
