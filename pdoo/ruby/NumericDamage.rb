require_relative "Damage.rb"
require_relative "lib/NumericDamageToUI.rb"

module Deepspace
  class NumericDamage<Damage
    attr_reader :nWeapons
    
    def initialize(w,s)
      super(s)
      @nWeapons = w
    end

    def copy
      new NumericDamage(@nWeapons,@nShields)
    end

    def adjust(w,s)
      newShields = [@nShields,s.length].min
      newWeapons = [@nWeapons,w.length].min
      NumericDamage.new(newWeapons,newShields)
    end

    def discardWeapon(w)
      if @nWeapons>0
        @nWeapons-=1
      end
    end

    def hasNoEffect
      return (@nWeapons==0 and super)
    end

    def getUIversion
      NumericDamageToUI.new(self)
    end
  end
end
