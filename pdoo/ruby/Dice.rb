#encoding: UTF-8

require_relative "GameCharacter.rb"

module Deepspace
  class Dice
    def initialize
      @NHANGARSPROB=0.25
      @NSHIELDSPROB=0.25
      @NWEAPONSPROB=0.33
      @FIRSTSHOTPROB=0.5
      @EXTRAEFFICIENCYPROB=0.8
      @generator=Random.new
    end
    def initWithNHangars
      if (@generator.rand(1.0)<@NHANGARSPROB)
        return 0
      else
        return 1
      end
    end
    def initWithNShields
      if (@generator.rand(1.0)<@NSHIELDSPROB)
        return 0
      else
        return 1
      end
    end
    def initWithNWeapons
      if (@generator.rand(1.0)<@NWEAPONSPROB)
        return 1
      elsif (@generator.rand(1.0)<@NWEAPONSPROB)
        return 2
      else
        return 3
      end
    end
    def whoStarts(nPlayers)
      return @generator.rand(nPlayers-1)
    end
    def firstShot
      if (@generator.rand(1.0)<@FIRSTSHOTPROB)
        return GameCharacter::SPACESTATION
      else
        return GameCharacter::ENEMYSTARSHIP
      end
    end
    def spaceStationMoves(speed)
      if (@generator.rand(1.0)<speed)
        return true
      else
        return false
      end
    end

    def extraEfficiency
      return (@generator.rand(1.0)<@EXTRAEFFICIENCYPROB)
    end

    def to_s
      "FIRSTSHOTPROB=#{@FIRSTSHOTPROB}\n
      NHANGARSPROB=#{@NHANGARSPROB}\n
      NSHIELDSPROB=#{@NSHIELDSPROB}\n
      NWEAPONSPROB=#{@NWEAPONSPROB}"
    end
  end
end
