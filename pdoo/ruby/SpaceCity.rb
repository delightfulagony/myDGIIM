require_relative "SpaceStation.rb"

module Deepspace
  class SpaceCity<SpaceStation
    attr_reader :collaborators

    def initialize(base,collabs)
      newCopy(base)
      @base = base
      @collaborators = collabs
      puts "Has GANADO y tu estación se ha transformado en una SpaceCity!"
    end

    def fire
      shot=super
      @collaborators.each do |s|
        shot+=s.fire
      end
      return shot
    end

    def protection
      shield = super
      @collaborators.each do |s|
        shield+=s.protection
      end
      return shield
    end

    def setLoot(loot)
      super
      Transformation::NOTRANSFORM
    end

    def to_s
      message = "#==Ciudad Espacial==#\n"+"Base principal:\n"
      message +=super
      message +="\nApoyos\n"
      @collaborators.each do |s|
        aux += s.to_s
      end
      return message
    end
  end
end
