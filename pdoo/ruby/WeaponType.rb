# encoding: UTF-8
module Deepspace
  module WeaponType
   class Type
     def initialize(p,n)
       @power=p
       @name=n
     end

     def to_s
       "name=#{@name}\n"+
       "power=#{@power}"
     end

     public

     def power
       return @power
     end
   end
   LASER= Type.new(2.0,"LASER")
   MISSILE= Type.new(3.0,"MISSILE")
   PLASMA= Type.new(4.0,"PLASMA")
  end
end
