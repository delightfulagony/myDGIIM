#include: UTF-8

require_relative "lib/CardDealer.rb"
require_relative "lib/GameState.rb"
require_relative "lib/GameStateController.rb"
require_relative "lib/GameUniverseToUI.rb"
require_relative "Dice.rb"
require_relative "SpaceStation.rb"
require_relative "EnemyStarShip.rb"
require_relative "CombatResult.rb"
require_relative "SpaceCity.rb"
require_relative "PowerEfficientSpaceStation.rb"
require_relative "BetaPowerEfficientSpaceStation.rb"
require_relative "Transformation.rb"

module Deepspace
  class GameUniverse

    @@WIN=10

    def initialize
      @currentStationIndex=0
      @turns=0
      @dice=Dice.new
      @currentEnemy=nil
      @currentStation=nil
      @spaceStations=Array.new
      @gameState=GameStateController.new
      @haveSpaceCity=false
    end

    def combatGo(station,enemy)
      ch=@dice.firstShot
      if ch==GameCharacter::ENEMYSTARSHIP
        fire=enemy.fire
        result=station.receiveShot(fire)
        if result==ShotResult::RESIST
          fire=station.fire
          result=enemy.receiveShot(fire)
          enemyWins=(result==ShotResult::RESIST)
        else
          enemyWins=true
        end
      else
        fire=station.fire
        result=enemy.receiveShot(fire)
        enemyWins=(result==ShotResult::RESIST)
      end
      if enemyWins
        s=station.getSpeed
        moves=@dice.spaceStationMoves(s)
        if !moves
          damage=enemy.getDamage
          station.setPendingDamage(damage)
          combatResult=CombatResult::ENEMYWINS
        else
          station.move
          combatResult=CombatResult::STATIONESCAPES
        end
      else
        aLoot=enemy.getLoot
        transform = station.setLoot(aLoot)
        if transform == Transformation::GETEFFICIENT
          makeStationEfficient
          combatResult = CombatResult::STATIONWINSANDCONVERTS
        elsif transform == Transformation::SPACECITY
          createSpaceCity
          combatResult = CombatResult::STATIONWINSANDCONVERTS
        else
          combatResult=CombatResult::STATIONWINS
        end
      end
      @gameState.next(@turns,@spaceStations.size)
      combatResult
    end

    def getUIversion
      GameUniverseToUI.new(@currentStation, @currentEnemy)
    end

    def to_s
      getUIversion.to_s
    end

    public

    def combat
      state=getState
      if state==GameState::BEFORECOMBAT or state==GameState::INIT
        combatGo(@currentStation,@currentEnemy)
      else
        combatResult::NOCOMBAT
      end
    end

    def discardHangar
      @currentStation.discardHangar
    end

    def discardShieldBooster(i)
      if @gameState.state==GameState::INIT or @gameState.state==GameState::AFTERCOMBAT
        @currentStation.discardShieldBooster(i)
      end
    end

    def discardShieldBoosterInHangar(i)
      if @gameState.state==GameState::INIT or @gameState.state==GameState::AFTERCOMBAT
        @currentStation.discardShieldBoosterInHangar(i)
      end
    end

    def discardWeapon(i)
      if @gameState.state==GameState::INIT or @gameState.state==GameState::AFTERCOMBAT
        @currentStation.discardWeapon(i)
      end
    end
    
    def discardWeaponInHangar(i)
      if @gameState.state==GameState::INIT or @gameState.state==GameState::AFTERCOMBAT
        @currentStation.discardWeaponInHangar(i)
      end
    end

    def getState
      @gameState.state
    end

    def state
      getState
    end

    def haveAWinner
      @currentStation.getNMedals>=@WIN
    end

    def init(names)
      state=getState
      if state==GameState::CANNOTPLAY
        dealer=CardDealer.instance
        names.each do |n|
          supplies=dealer.nextSuppliesPackage
          station=SpaceStation.new(n,supplies)
          @spaceStations << station
          nh=@dice.initWithNHangars
          nw=@dice.initWithNWeapons
          ns=@dice.initWithNShields
          lo=Loot.new(0,nw,ns,nh,0)
          station.setLoot(lo)
        end
        @currentStationIndex=@dice.whoStarts(names.size)
        @currentStation=@spaceStations[@currentStationIndex]
        @currentEnemy=dealer.nextEnemy
        @gameState.next(@turns,@spaceStations.size)
      end
    end

    def mountShieldBooster(i)
      if @gameState.state==GameState::INIT or @gameState.state==GameState::AFTERCOMBAT
        @currentStation.mountShieldBooster(i)
      end
    end

    def mountWeapon(i)
      if @gameState.state==GameState::INIT or @gameState.state==GameState::AFTERCOMBAT
        @currentStation.mountWeapon(i)
      end
    end

    def nextTurn
      state=getState
      if state==GameState::AFTERCOMBAT
        stationState=@currentStation.validState
        if stationState
          @currentStationIndex=(@currentStationIndex+1) % @spaceStations.size
          @turns=@turns+1
          @currentStation=@spaceStations[@currentStationIndex]
          @currentStation.cleanUpMountedItems
          dealer=CardDealer.instance
          @currentEnemy=dealer.nextEnemy
          @gameState.next(@turns,@spaceStations.size)
          return true
        end
        return false
      end
      return false
    end

    def makeStationEfficient
      if @dice.extraEfficiency
        @currentStation = BetaPowerEfficientSpaceStation.new(@currentStation)
      else
        @currentStation = PowerEfficientSpaceStation.new(@currentStation)
      end
    end

    def createSpaceCity
      if @haveSpaceCity==false
        collaborators = @spaceStations.select {|station| station!=@currentStation}
        @currentStation = SpaceCity.new(@currentStation,collaborators)
        @haveSpaceCity = true
      end
    end

  end
end
