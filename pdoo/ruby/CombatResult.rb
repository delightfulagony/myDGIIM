# encoding: UTF-8
module Deepspace
  module CombatResult
    ENEMYWINS= :enemywins
    NOCOMBAT= :nocombat
    STATIONESCAPES= :stationescapes
    STATIONWINS= :stationwins
    STATIONWINSANDCONVERTS= :stationwindsandconverts
  end
end

