#encoding: UTF-8

require_relative "../SpaceStation.rb"
require_relative "../SuppliesPackage.rb"
require_relative "../Hangar.rb"
require_relative "../Weapon.rb"
require_relative "../WeaponType.rb"
require_relative "../ShieldBooster.rb"

module Deepspace
  class SpaceStationTest
    def self.main
      puts "\tCreamos un objeto de la clase SpaceStation"
      sp1=SuppliesPackage.new(0.0,1.0,0.6)
      ss1=SpaceStation.new("Aventurero galáctico",sp1)
      puts "  ss1:\n\t" + ss1.to_s

      puts "\tAñadimos un hangar a ss1"
      h1=Hangar.new(10)
      w1=Weapon.new("Destruye destructores",WeaponType::PLASMA,3)
      h1.addWeapon(w1)
      w2=Weapon.new("Cañón del amor",WeaponType::LASER,1)
      h1.addWeapon(w2)
      s2=ShieldBooster.new("Escudo Ratchet",0.4,2)
      h1.addShieldBooster(s2)

      ss1.receiveHangar(h1)
      puts "  ss1:\n\t" + ss1.to_s

      puts "\tProbamos SpaceStation::receiveWeapon y SpaceStation::receiveShieldBooster"
      w3=Weapon.new("Misil teledirigido",WeaponType::MISSILE,8)
      s3=ShieldBooster.new("Protección de iones",0.7,1)
      ss1.receiveWeapon(w3)
      ss1.receiveShieldBooster(s3)
      puts "  ss1:\n\t" + ss1.to_s

      puts "\tAsignamos un pendingDamage a ss1 y comprobamos si se encuentra en un estado válido"
      d1=Damage.newNumericWeapons(3,2)
      ss1.setPendingDamage(d1)
      puts "  ss1:\n\t" + ss1.to_s
      puts "¿Estado válido?"
      puts ss1.validState

      puts "\tEquipamos las armas y escudos"
      ss1.getHangar.getWeapons.length.times do
        ss1.mountWeapon(0)
      end
      ss1.getHangar.getShieldBoosters.length.times do
        ss1.mountShieldBooster(0)
      end
      puts "  ss1:\n\t" + ss1.to_s

      puts "\tProbamos a asignar ahora pendingDamage con daño numérico"
      d2=Damage.newNumericWeapons(20,12)
      ss1.setPendingDamage(d2)
      puts "  ss1:\n\t" + ss1.to_s

      puts "\tProbamos a asignar ahora pendingDamage con daño específico"
      d3=Damage.newSpecificWeapons([w1,w2],1)
      ss1.setPendingDamage(d3)
      puts "  ss1:\n\t" + ss1.to_s

      puts "\tDescartamos el hangar una vez equipadas las armas"
      ss1.discardHangar
      puts "  ss1:\n\t" + ss1.to_s

      puts "\tProbamos los observadores"
      puts "getAmmoPower:#{ss1.getAmmoPower}\n"
      puts "getFuelUnits:#{ss1.getFuelUnits}\n"
      puts "getName:#{ss1.getName}\n"
      puts "getNMedals:#{ss1.getNMedals}\n"
      puts "getShieldPower:#{ss1.getShieldPower}\n"
      puts "getSpeed:#{ss1.getSpeed}\n"

      puts "\tDesplazamos por medio de SpaceStation::move al objeto"
      ss1.move
      puts "getFuelUnits:#{ss1.getFuelUnits}\n"
      puts "getSpeed:#{ss1.getSpeed}\n"

      puts "\tProbamos SpaceStation::receiveSupplies"
      sp2=SuppliesPackage.new(0.5,1.3,2.6)
      ss1.receiveSupplies(sp2)
      puts "  ss1:\n\t" + ss1.to_s

      puts "\tComprobamos que SpaceStation::validState devuelve false"
      puts ss1.validState
      
      puts "\tPor último comprobamos SpaceStation::cleanUpMountedItems"
      ss1.cleanUpMountedItems
      puts "  ss1:\n\t" + ss1.to_s
    end
  end
end

Deepspace::SpaceStationTest.main
