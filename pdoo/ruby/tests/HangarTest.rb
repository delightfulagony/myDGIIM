#encoding: UTF-8

require_relative "../Hangar.rb"
require_relative "../WeaponType.rb"

module Deepspace
  class HangarTest
    def self.main
      puts "\tCreamos un hangar con capacidad 10 y una copia suya"
      h1=Hangar.new(10)
      h2=Hangar.newCopy(h1)
      puts "h1=\n"+h1.to_s
      puts "h2=\n"+h2.to_s

      puts "\tAñadimos un objeto Weapon a h1"
      w1=Weapon.new("Destruye destructores",WeaponType::PLASMA,3)
      h1.addWeapon(w1)
      puts "  h1:\n"+h1.to_s
      puts "  h2:\n"+h2.to_s

      puts "\tCreamos un hangar copia de el que hemos añadido el arma"
      h3=Hangar.newCopy(h1)
      puts "  h1:\n"+h1.to_s
      puts "  h3:\n"+h3.to_s

      puts "\tAñadimos un objeto ShieldBooster a h2"
      s1=ShieldBooster.new("Barrera Protónica",0.9,4)
      h2.addShieldBooster(s1)
      puts "  h2:\n"+h2.to_s

      puts "\tAñadimos mas armas y escudos a h1"
      w2=Weapon.new("Cañón del amor",WeaponType::LASER,1)
      h1.addWeapon(w2)
      w3=Weapon.new("Misil teledirigido",WeaponType::MISSILE,8)
      h1.addWeapon(w3)
      s2=ShieldBooster.new("Escudo Ratchet",0.4,2)
      h1.addShieldBooster(s2)
      s3=ShieldBooster.new("Protección de iones",0.7,1)
      h1.addShieldBooster(s3)
      puts "  h1:\n"+h1.to_s

      puts "\tEliminamos el 2o escudo y la 2a arma"
      h1.removeShieldBooster(1)
      h1.removeWeapon(1)
      puts "  h1:\n"+h1.to_s
    end
  end
end

Deepspace::HangarTest.main
