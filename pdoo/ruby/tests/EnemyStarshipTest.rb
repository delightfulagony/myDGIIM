#encoding: UTF-8

require_relative "../EnemyStarship.rb"
require_relative "../Loot.rb"
require_relative "../Damage.rb"
require_relative "../ShotResult.rb"

module Deepspace
  class EnemyStarshipTest
    def self.main
      puts "\tCreamos una nave y su copia y la imprimimos por pantalla"
      l1=Loot.new(5,6,3,2,0)
      d1=Damage.newNumericWeapons(3,2)
      es1=EnemyStarship.new("Destructor de planetas",4,2,l1,d1)
      es2=EnemyStarship.newCopy(es1)
      puts "  es1:\n"+es1.to_s
      puts "  es2:\n"+es2.to_s

      puts "\n\tLos siguientes métodos deberían ser iguales 2 a 2"
      print "es1::protection:\n\t" 
      puts es1.protection
      print "es1::getShieldPower:\n\t" 
      puts es1.getShieldPower
      print "es1::fire:\n\t" 
      puts es1.fire
      print "es1::getAmmoPower:\n\t" 
      puts es1.getAmmoPower

      puts "\tComprobamos los observadores que faltan"
      print "es1::getName:\n\t"
      puts es1.getName
      print "es1::getDamage:\n"
      puts es1.getDamage
      print "es1::getLoot:\n"
      puts es1.getLoot

      puts "\tComprobamos por ultimo EnemyStarship::receiveShot"
      puts "\tes2 va a recibir un disparo con potencia 0.5"
      puts es2.receiveShot(0.5)
      puts "\tAhora recibirá uno con potencia 10"
      puts es2.receiveShot(10)
    end
  end
end

Deepspace::EnemyStarshipTest.main
