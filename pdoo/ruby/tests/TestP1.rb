# encoding: UTF-8
require_relative "./CombatResult.rb"
require_relative "./Dice.rb"
require_relative "./GameCharacter.rb"
require_relative "./Loot.rb"
require_relative "./ShieldBooster.rb"
require_relative "./ShotResult.rb"
require_relative "./SuppliesPackage.rb"
require_relative "./Weapon.rb"
require_relative "./WeaponType.rb"

module Deepspace
  class TestP1
    def self.main
      # Test clase Loot
      puts "Loot:"
      loot1=Loot.new(5,6,3,2,0)
      loot2=Loot.new(0,0,0,0,0)
      puts loot1.nSupplies
      puts loot2.nWeapons
      puts loot2.nShields
      puts loot1.nHangars
      puts loot1.nMedals

      # Test clase ShieldBooster
      puts "ShieldBooster:"
      shieldBooster1=ShieldBooster.new("Super escudo",0.6,5)
      shieldBooster2=ShieldBooster.new("Escudo cutre",0.2,2)
      shieldBoosterCopia1=ShieldBooster.newCopy(shieldBooster1)
      print shieldBooster1.boost
      print "=="
      puts shieldBoosterCopia1.boost
      print "Uses: "
      puts shieldBooster2.uses
      shieldBooster2.useIt
      print "Tras usarlo: "
      puts shieldBooster2.uses

      # Test clase SuppliesPackage
      puts "SuppliesPackage:"
      suppliesPackage1=SuppliesPackage.new(0.0,1.0,0.6)
      suppliesPackage2=SuppliesPackage.new(1.0,1.0,1.0)
      suppliesPackageCopia2=SuppliesPackage.newCopy(suppliesPackage2)

      # Test clase Weapon
      puts "Weapon:"
      weapon1=Weapon.new("Destruye destructores",WeaponType::PLASMA,3)
      weapon2=Weapon.new("Cañón gastado",WeaponType::LASER,2)
      weaponCopia2=Weapon.newCopy(weapon2)
      puts "Weapon 1:"
      print "Type: "
      puts weapon1.getType
      print "Uses: "
      puts weapon1.getUses
      print "Power: "
      puts weapon1.power
      weapon1.useIt
      puts "Tras su uso:"
      print "Uses: "
      puts weapon1.getUses
      puts "Comparamos Weapon 2 con su copia"
      puts weapon2.getType + "==" + weaponCopia2.getType
      puts weapon2.getUses + "==" + weaponCopia2.getUses
      puts weapon2.power + "==" + weaponCopia2.power
      puts "Tras usar la copia:"
      weaponCopia2.useIt
      puts weapon2.getUses + ">" + weaponCopia2.getUses

      # Test clase Dice
      puts "Dice: "
      nHangarsTotal=0
      nShieldsTotal=0
      nWeaponsTotal1=0
      nWeaponsTotal2=0
      firstShotTotalEnemy=0
      dado=Dice.new
      for i in 100
        if (dado.initWithNHangars==1)
          nHangarsTotal+=1 end
        if (dado.initWithNShields==1) 
          nShieldsTotal+=1 end
        resultDadoNWeapons=dado.nWeapons
        if (resultDadoNWeapons==1) 
          nWeaponsTotal1+=1
        elsif (resultDadoNWeapons==2) 
          nWeaponsTotal2+=1 end
        if (dado.firstShot==GameCharacter::ENEMYSTARSHIP)
          firstShotTotalEnemy+=1 end
      end
    end
  end
end

Deepspace::TestP1.main
