#encoding: UTF-8

require_relative "../Damage.rb"
require_relative "../Weapon.rb"
require_relative "../ShieldBooster.rb"

module Deepspace
  class DamageTest
    def self.main
      puts "\tCreamos un objeto de tipo Damage y una copia de éste"
      d1=Damage.newNumericWeapons(3,2)
      d2=Damage.newCopy(d1)
      puts "  d1:\n" + d1.to_s
      puts "  d2:\n" + d1.to_s
      
      puts "\tDescartamos 1 Weapon y 1 ShieldBooster de d1"
      d1.discardWeapon(1)
      d1.discardShieldBooster
      puts "  d1:\n" + d1.to_s

      puts "\tCreamos un objeto de tipo Damage con un array de Weapon"
      w1=Weapon.new("Destruye destructores",WeaponType::PLASMA,3)
      w2=Weapon.new("Cañón del amor",WeaponType::LASER,1)
      w3=Weapon.new("Misil teledirigido",WeaponType::MISSILE,8)
      d3=Damage.newSpecificWeapons([w1,w2,w3],4)
      puts "  d3:\n" + d3.to_s

      puts "\tCopiamos el objeto creado anteriormente"
      d4=Damage.newCopy(d3)
      puts "  d4:\n" + d4.to_s

      puts "\tEliminamos el Cañón del amor de la copia"
      d4.discardWeapon(w2)
      puts "  d4:\n" + d4.to_s

      puts "\tEjecutamos d2::adjust que deberia eliminar 1 weapon"
      s1=ShieldBooster.new("Barrera Protónica",0.9,4)
      s2=ShieldBooster.new("Barrera Iónica",0.9,4)
      d2.adjust([w2,w3],[s1,s2])
      puts "  d2:\n" + d2.to_s
      
      puts "\tEjecutamos d3::Adjust que debería eliminar el Destruye destructores y 2 escudos"
      d3.adjust([w2,w3],[s1,s2])
      puts "  d3:\n" + d3.to_s

      puts "\tCreamos un objeto Damage vacío y comprobamos si el método Damage::hasNoEffect devuelve true correctamente y lo comparamos con d4"
      d5=Damage.newNumericWeapons(0,0)
      puts "  d5:\n" + d5.to_s
      print "\nd4::hasNoEffect: "+ d4.hasNoEffect.to_s
      print "\nd5::hasNoEffect: "+ d5.hasNoEffect.to_s
      print "\n"
    end
  end
end

Deepspace::DamageTest.main
