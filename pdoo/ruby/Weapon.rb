# encoding: UTF-8

require_relative "lib/WeaponToUI.rb"

module Deepspace
  class Weapon
    attr_reader :name, :type, :uses
    def initialize(_name, _type, _uses)
      @name=_name
      @type=_type
      @uses=_uses
    end

    def self.newCopy(_weapon)
      new(_weapon.name, _weapon.type, _weapon.uses)
    end

    def getUIversion
      WeaponToUI.new(self)
    end

    def to_s
      getUIversion.to_s
    end

    public

    def getType()
      return @type
    end

    def getUses()
      return @uses
    end

    def power()
      return @type.power()
    end

    def useIt
      if (@uses>0)
        @uses-=1
        return power()
      else
        return 1.0
      end
    end
  end
end

