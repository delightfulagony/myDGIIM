#encoding: UTF-8

require_relative "lib/HangarToUI.rb"
require_relative "ShieldBooster.rb"
require_relative "Weapon.rb"

module Deepspace
  class Hangar
    attr_reader :maxElements, :weapons, :shieldBoosters
    def initialize(capacity)
      @maxElements = capacity
      @weapons = Array.new
      @shieldBoosters = Array.new
    end

    def self.newCopy(h)
      copy = self.new(h.getMaxElements)
      h.getWeapons.each { |w| copy.addWeapon(w) }
      h.getShieldBoosters.each { |s| copy.addShieldBooster(s) }
      copy
    end

    def getUIversion
      HangarToUI.new(self)
    end

    def to_s
      getUIversion.to_s
    end
    
    private

    def spaceAvailable
      (@weapons.length+@shieldBoosters.length)<@maxElements
    end

    public

    def addWeapon(w)
      if spaceAvailable
        @weapons << w
      end
    end

    def addShieldBooster(s)
      if spaceAvailable
        @shieldBoosters << s  
      end
    end

    def getMaxElements
      @maxElements
    end

    def getShieldBoosters
      @shieldBoosters
    end

    def getWeapons
      @weapons
    end

    def removeWeapon(w)
      if @weapons != nil
        @weapons.delete_at(w)
      end
    end

    def removeShieldBooster(s)
      if @shieldBoosters != nil
        @shieldBoosters.delete_at(s)
      end
    end
  end
end
