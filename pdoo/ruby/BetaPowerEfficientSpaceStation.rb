require_relative "PowerEfficientSpaceStation.rb"
require_relative "Dice.rb"

module Deepspace
  class BetaPowerEfficientSpaceStation<PowerEfficientSpaceStation
    @@EXTRAEFFICIENCY = 1.2

    def initialize(s)
      newCopy(s)
      transformText
    end

    def transformText
      puts "Has GANADO y tu estación se ha transformado en una PowerEfficientSpaceStation!"
    end

    def fire
      dice = Dice.new
      if dice.extraEfficiency
        return super*@@EXTRAEFFICIENCY
      else
        return super
      end
    end
  end
end
