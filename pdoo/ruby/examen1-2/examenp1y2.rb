#encoding: UTF-8

require_relative "figura.rb"
require_relative "../WeaponType.rb"
require_relative "../Weapon.rb"
require_relative "../Damage.rb"
require_relative "../ShieldBooster.rb"

module ModuloExamen
  class ExamenP1y2
    def self.main
      figuras=Array.new
      figuras << Figura.circulo(1)
      figuras << Figura.circulo(2)
      figuras << Figura.rectangulo(2,3)
      figuras << Figura.rectangulo(3,4)
      
      puts "Figuras:"
      puts figuras
      
      print "Sumatoria de las áreas:\n\t"
      sum=0
      figuras.each{|f| sum+=f.area}
      puts sum
      
      print "Área media de las figuras:\n\t"
      puts sum/Figura.getNFiguras
      
      puts "EJERCICIO 4:"
      d1=Deepspace::Damage.newSpecificWeapons(
        [Deepspace::WeaponType::LASER,
        Deepspace::WeaponType::MISSILE,
        Deepspace::WeaponType::MISSILE],
        1)
      print "Daño sin ajustar:\n"
      puts d1.to_s
      print "Daño ajustado:\n"
      puts d1.adjust(
        [Deepspace::Weapon.new("Arma1",Deepspace::WeaponType::LASER,2),
        Deepspace::Weapon.new("Arma2",Deepspace::WeaponType::LASER,3),
        Deepspace::Weapon.new("Arma3",Deepspace::WeaponType::MISSILE,1),
        Deepspace::Weapon.new("Arma4",Deepspace::WeaponType::PLASMA,4)],
        [Deepspace::ShieldBooster.new("Escudo1",1.3,3),
        Deepspace::ShieldBooster.new("Escudo2",0.4,2)])
    end
  end
end

ModuloExamen::ExamenP1y2.main
