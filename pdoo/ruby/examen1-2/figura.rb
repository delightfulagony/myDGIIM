#encoding: UTF-8

module ModuloExamen
  class Figura
    @@nFiguras=0
    private def initialize(r,l1,l2)
      @radio = r    
      @lado1 = l1
      @lado2 = l2
      @@nFiguras+=1
    end
    
    def self.circulo(r)
      self.new(r,nil,nil)
    end
    def self.rectangulo(l1,l2)
      self.new(nil,l1,l2)
    end
    
    def to_s
      if @radio==nil
        "rectangulo:\n"+
        "\tlado1=#{@lado1}\n"+
        "\tlado2=#{@lado2}\n"
      else
        "circulo:\n"+
        "\tradio=#{@radio}\n"
      end
    end
    
    def area
      if @radio==nil
        @lado1*@lado2
      else
        Math::PI*(@radio)*(@radio)
      end
    end
    
    def self.getNFiguras
      @@nFiguras
    end
  end
end
