# encoding: UTF-8
module Deepspace
  class SuppliesPackage
    attr_reader :ammoPower, :fuelUnits, :shieldPower
    def initialize(_ammoPower, _fuelUnits, _shieldPower)
      @ammoPower=_ammoPower
      @fuelUnits=_fuelUnits
      @shieldPower=_shieldPower
    end

    def self.newCopy(_suppliesPackage)
      new(_suppliesPackage.ammoPower, _suppliesPackage.fuelUnits, _suppliesPackage.shieldPower)
    end

    def to_s
      "ammoPower=#{@ammoPower}\n"+
      "fuelUnits=#{@fuelUnits}\n"+
      "shieldPower=#{@shieldPower}\n"
    end

    public

    def getAmmoPower
      @ammoPower
    end

    def getFuelUnits
      @fuelUnits
    end

    def getShieldPower
      @shieldPower
    end
  end
end
