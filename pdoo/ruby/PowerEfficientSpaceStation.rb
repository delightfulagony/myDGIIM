require_relative "SpaceStation.rb"

module Deepspace
  class PowerEfficientSpaceStation<SpaceStation

    @@EFFICIENCYFACTOR = 1.10

    def initialize(s)
      newCopy(s)
      transformText
    end

    def transformText
      puts "Has GANADO y tu estación se ha transformado en una PowerEfficientSpaceStation!"
    end

    def fire
      return @@EFFICIENCYFACTOR*super
    end
    
    def protection
      return @@EFFICIENCYFACTOR*super
    end

    def setLoot(loot)
      super
      if loot.getEfficient==true
        return Transformation::GETEFFICIENT
      else
        return Transformation::NOTRANSFORM
      end
    end

    def to_s
      message = "#==PowerEfficientSpaceStation==#\n"
      message +=super
      return message
    end
  end
end
