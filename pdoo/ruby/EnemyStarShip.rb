#encoding: UTF-8

require_relative "lib/EnemyToUI.rb"
require_relative "Damage.rb"
require_relative "Loot.rb"

module Deepspace
  class EnemyStarShip
    attr_reader :ammoPower, :name, :shieldPower, :loot, :damage

    def initialize(n, a, s, l, d)
      @name = n
      @ammoPower = a
      @shieldPower = s
      @loot = l
      @damage = d
    end

    def self.newCopy(e)
      self.new(e.name,e.ammoPower,e.shieldPower,e.loot,e.damage)
    end

    def getUIversion
      EnemyToUI.new(self)
    end

    def to_s
      EnemyToUI.to_s
    end

    public

    def fire
      @ammoPower
    end

    def getAmmoPower
      @ammoPower
    end

    def getDamage
      @damage
    end

    def getLoot
      @loot
    end

    def getName
      @name
    end

    def getShieldPower
      @shieldPower
    end

    def protection
      @shieldPower
    end

    def receiveShot(shot)
      shot>@shieldPower?(ShotResult::DONOTRESIST):(ShotResult::RESIST)
    end
  end
end
