require_relative "Damage.rb"
require_relative "Weapon.rb"
require_relative "lib/SpecificDamageToUI.rb"

module Deepspace
  class SpecificDamage<Damage
    attr_reader :weapons

    def initialize(wl,s)
      super(s)
      @weapons = wl
    end

    def copy
      new SpecificDamage(@weapons,@nShields)
    end

    def getUIversion
      SpecificDamageToUI.new(self)
    end

    public

    def adjust(w,s)
      newShields = [@nShields,s.length].min
      newWeapons = Array.new(@weapons)

      newWeapons.each_with_index do |wt,i|
        if(arrayContainsType(w,wt)==-1)
          newWeapons[i] = nil
        else
          index = arrayContainsType(w,wt)
          wl = Array.new
          w.each_index do |i|
            if i>=index
              wl << w[i].type
            end
          end
          if (newWeapons.count(wt)>wl.count(wt))
            newWeapons[i] = nil
          end
        end
      end
      newWeapons = newWeapons.compact
      SpecificDamage.new(newWeapons,newShields)
    end

    def discardWeapon(w)
      type = w.type
      index = -1
      @weapons.each_index do |i|
        if @weapons[i]==type
          index = i
        end
      end
      if index!=-1
        @weapons.delete_at(index)
      end
    end

    def hasNoEffect
      return (@weapons.length==0 and super)
    end

    private
    
    def arrayContainsType(wl,t)
      wl.each_index do |i|
        if wl[i].type==t
          return i
        end
      end
      return -1
    end
  end
end
