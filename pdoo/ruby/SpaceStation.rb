#encoding: UTF-8

require_relative "lib/SpaceStationToUI.rb"
require_relative "Damage.rb"
require_relative "Weapon.rb"
require_relative "ShieldBooster.rb"
require_relative "Hangar.rb"
require_relative "SuppliesPackage.rb"
require_relative "ShotResult.rb"
require_relative "Transformation.rb"

module Deepspace
  class SpaceStation
    attr_reader :weapons, :shieldBoosters, :name, :nMedals, :ammoPower, :fuelUnits, :shieldPower, :hangar, :pendingDamage
    
    @@MAXFUEL = 100
    @@SHIELDLOSSPERUNITSHOT = 0.1
    def initialize(name, supplies) 
      @name = name
      @fuelUnits = [@@MAXFUEL, supplies.getFuelUnits].max
      @ammoPower = supplies.getAmmoPower
      @shieldPower = supplies.getShieldPower
      @nMedals = 0
      @weapons = Array.new
      @shieldBoosters = Array.new
      @hangar = nil 
      @pendingDamage = nil
    end

    def newCopy(s)
      @name = s.name
      @ammoPower = s.ammoPower
      @fuelUnits = s.fuelUnits
      @shieldPower = s.shieldPower
      @nMedals = s.nMedals
      @weapons = s.weapons
      @shieldBoosters = s.shieldBoosters
      @pendingDamage = s.pendingDamage
      @hangar = s.hangar
    end

    def getUIversion
      SpaceStationToUI.new(self)
    end

    def to_s
      getUIversion.to_s
    end

    public

    def cleanUpMountedItems
      @weapons.each {|w|
        if w.getUses<1
          @weapons.delete(w)
        end
      }
      @shieldBoosters.each {|s|
        if s.getUses<1
          @shieldBoosters.delete(s)
        end
      }
    end

    def discardHangar
      @hangar = nil
    end

    def discardShieldBooster(i)
      size = @shieldBoosters.size
      if i>=0 and i<size
        s=@shieldBoosters.delete_at(i)
        if @pendingDamage!=nil
          @pendingDamage.discardShieldBooster
          cleanPendingDamage
        end
      end
    end

    def discardShieldBoosterInHangar(i)
      if @hangar != nil
        @hangar.removeShieldBooster(i)
      end
    end

    def discardWeapon(i)
      size = @weapons.size
      if i>=0 and i<size
        w=@weapons.delete_at(i)
        if @pendingDamage!=nil
          @pendingDamage.discardWeapon(w)
          cleanPendingDamage
        end
      end
    end

    def discardWeaponInHangar(i)
      if @hangar != nil
        @hangar.removeWeapon(i)
      end
    end

    def fire
      factor=1.0
      @weapons.each {|w| factor=factor*w.useIt}
      @ammoPower*factor
    end

    def getAmmoPower
      @ammoPower
    end

    def getFuelUnits
      @fuelUnits
    end
    
    def getHangar
      @hangar
    end

    def getName
      @name
    end

    def getNMedals
      @nMedals
    end

    def getPendingDamage
      @pendingDamage
    end

    def getShieldBoosters
      @shieldBoosters
    end

    def getShieldPower
      @shieldPower
    end

    def getSpeed
      @fuelUnits/@@MAXFUEL
    end

    def getWeapons
      @weapons
    end

    def mountShieldBooster(i)
      s=nil
      if(@hangar!=nil)
        s=@hangar.removeShieldBooster(i)
        if(s!=nil)
          @shieldBoosters.push(s)
        end
      end
    end

    def mountWeapon(i)
      w=nil
      if(@hangar!=nil)
        w=@hangar.removeWeapon(i)
        if(w!=nil)
          @weapons.push(w)
        end
      end
    end

    def move
      @fuelUnits=@fuelUnits - getSpeed
      if @fuelUnits<0
        @fuelUnits=0
      end
    end

    def protection
      factor=1.0
      @shieldBoosters.each{|s| factor=factor*s.useIt}
      @shieldPower*factor
    end

    def receiveHangar(h)
      if @hangar==nil
        @hangar=h
      end
    end

    def receiveShieldBooster(s)
      if @hangar!=nil
        @hangar.addShieldBooster(s)
      else
        false
      end
    end

    def receiveShot(shot)
      myProtection=protection
      if myProtection>=shot
        @shieldPower=@shieldPower-@@SHIELDLOSSPERUNITSHOT*shot
        @shieldPower=[0.0, @shieldPower].max
        ShotResult::RESIST
      else
        @shieldPower=0.0
        ShotResult::DONOTRESIST
      end
    end

    def receiveSupplies(s)
      @ammoPower+=s.getAmmoPower
      @fuelUnits+=s.getFuelUnits
      @shieldPower+=s.getShieldPower
    end

    def receiveWeapon(w)
      if @hangar!=nil
        @hangar.addWeapon(w)
      else
        false
      end
    end

    #TODO: TERMINAR LA IMPLEMENTACIÓN
    def setLoot(loot)
      dealer = CardDealer.instance
      h = loot.getNHangars
      if h > 0
        hangar = dealer.nextHangar
        receiveHangar(hangar)
      end

      elements = loot.getNSupplies
      elements.times do |i|
        supply = dealer.nextSuppliesPackage
        receiveSupplies(supply)
      end

      elements = loot.getNWeapons
      elements.times do |i|
        weapon = dealer.nextWeapon
        receiveWeapon(weapon)
      end

      elements = loot.getNShields
      elements.times do |i|
        shield = dealer.nextShieldBooster
        receiveShieldBooster(shield)
      end

      medals = loot.getNMedals
      @nMedals = @nMedals + medals

      if loot.getEfficient==true
        return Transformation::GETEFFICIENT
      elsif loot.spaceCity==true
        return Transformation::SPACECITY
      else
        return Transformation::NOTRANSFORM
      end
    end

    def setPendingDamage(d)
      @pendingDamage = d.adjust(@weapons, @shieldBoosters)
    end

    def validState
      if @pendingDamage != nil
        @pendingDamage.hasNoEffect
      else
        true
      end
    end

    private

    def assignFuelValue(f)
      if f<=@MAXFUEL
        @fuelUnits=f
      end
    end

    def cleanPendingDamage
      if @pendingDamage==nil or @pendingDamage.hasNoEffect
        pendingDamage=nil;
      end
    end
  end
end
