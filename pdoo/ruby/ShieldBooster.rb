# encoding: UTF-8

require_relative "lib/ShieldToUI.rb"

module Deepspace
  class ShieldBooster
    attr_reader :name, :boost, :uses
    def initialize(_name, _boost, _uses)
      @name=_name
      @boost=_boost
      @uses=_uses
    end

    def self.newCopy(_shieldBooster)
      new(_shieldBooster.name, _shieldBooster.boost, _shieldBooster.uses)
    end

    def to_s
      getUIversion.to_s
    end

    public

    def getBoost
      return @boost
    end

    def getUses
      return @uses
    end

    def useIt
      if(@uses>0)
        @uses-=1
        return @boost
      else
        return 1.0
      end
    end

    def getUIversion
      return ShieldToUI.new(self)
    end
  end
end

