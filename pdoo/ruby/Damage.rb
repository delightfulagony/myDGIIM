#encoding: UTF-8

require_relative "lib/DamageToUI.rb"
require_relative "WeaponType.rb"

module Deepspace
  class Damage
    attr_reader :nShields 
    def initialize(s)
      @nShields = s
    end

    def to_s
      getUIversion.to_s
    end

    def discardShieldBooster
      if @nShields>0
        @nShields -= 1
      end
    end

    def hasNoEffect
      return @nShields==0
    end
  end
end
