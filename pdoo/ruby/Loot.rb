# encoding: UTF-8

require_relative "lib/LootToUI.rb"

module Deepspace
  class Loot
    attr_reader :nSupplies, :nWeapons, :nShields, :nHangars, :nMedals, :spaceCity, :efficient
    def initialize(_nSupplies, _nWeapons, _nShields, _nHangars, _nMedals, ef=false, city=false)
      @nSupplies=_nSupplies
      @nWeapons=_nWeapons
      @nShields=_nShields
      @nHangars=_nHangars
      @nMedals=_nMedals
      @efficient = ef
      @spaceCity = city
    end
    
    def getUIversion
      return LootToUI.new(self)
    end

    def to_s
      getUIversion.to_s
    end

    public

    def getEfficient
      @efficient
    end

    def getNSupplies
      return @nSupplies
    end

    def getNWeapons
      return @nWeapons
    end

    def getNShields
      return @nShields
    end

    def getNHangars
      return @nHangars
    end

    def getNMedals
      return @nMedals
    end

  end
end
