/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

import java.util.ArrayList;

/**
 *
 * @author agony
 */
public class SpaceStation implements SpaceFighter{
    private final static int MAXFUEL = 100;
    private final static float SHIELDLOSSPERUNITSHOT = (float)0.1;

    private String name;    
    private float fuelUnits;    
    private float ammoPower;
    private float shieldPower;
    private int nMedals = 0;
    private ArrayList<Weapon> weapons;
    private ArrayList<ShieldBooster> shieldBoosters;
    private Hangar hangar;
    private Damage pendingDamage;
    protected String type;

    SpaceStation(String n, SuppliesPackage supplies) {
        name = n;
        assignFuelValue(supplies.getFuelUnits());
        ammoPower = supplies.getAmmoPower();
        shieldPower = supplies.getShieldPower();
        nMedals = 0;
        weapons = new ArrayList<>();
        shieldBoosters = new ArrayList<>();
        hangar = null;
        pendingDamage = null;
        type = "Estación normal";
    }
    
    SpaceStation(SpaceStation station) {
        name = station.name;
        assignFuelValue(station.fuelUnits);
        ammoPower = station.ammoPower;
        shieldPower = station.shieldPower;
        nMedals = station.nMedals;
        weapons = station.weapons;
        shieldBoosters = station.shieldBoosters;
        hangar = station.hangar;
        pendingDamage = station.pendingDamage;
    }
    
    protected SpaceStationToUI getUIversion() {
        return new SpaceStationToUI(this);
    }
    
    public void cleanUpMountedItems() {
        if (weapons != null) {
            for (int i=0;i<weapons.size();i++) {
                if (weapons.get(i).getUses()<1) {
                    weapons.remove(i);
                    i--;
                }
            }
        }
        
        if (shieldBoosters != null) {
            for (int i=0;i<shieldBoosters.size();i++) {
                if (shieldBoosters.get(i).getUses()<1) {
                    shieldBoosters.remove(i);
                    i--;
                }
            }
        }
    }
    
    public void discardHangar() {
        hangar = null;
    }
    
    public void discardShieldBooster(int i) {
        if (i>=0 && i<shieldBoosters.size()) {
            shieldBoosters.remove(i);
            if (pendingDamage!=null) {
                pendingDamage.discardShieldBooster();
                cleanPendingDamage();
            }
        }
    }
    
    public void discardShieldBoosterInHangar(int i) {
        if (hangar!=null)
            hangar.removeShieldBoosters(i);
    }
    
    public void discardWeapon(int i) {
        if (i>=0 && i<weapons.size()) {
            Weapon w=weapons.remove(i);
            if (pendingDamage!=null) {
                pendingDamage.discardWeapon(w);
                cleanPendingDamage();
            }
        }
    }
    
    public void discardWeaponInHangar(int i) {
        if (hangar != null) {
            hangar.removeWeapon(i);
        }
    }
    
    @Override
    public float fire() {
        float factor=(float)1;
        for (Weapon w:weapons) {
            factor*=w.useIt();
        }
        return ammoPower*factor;
    }
    
    public float getAmmoPower() {
        return ammoPower;
    }
    
    public float getFuelUnits() {
        return fuelUnits;
    }
    
    public Hangar getHangar() {
        return hangar;
    }
    
    public String getName() {
        return name;
    }
    
    public int getNMedals() {
        return nMedals;
    }
    
    public Damage getPendingDamage() {
        return pendingDamage;
    }
    
    public ArrayList<ShieldBooster> getShieldBoosters() {
        return shieldBoosters;
    }
    
    public float getShieldPower() {
        return shieldPower;
    }
    
    public float getSpeed() {
        return fuelUnits/MAXFUEL;
    }
    
    public ArrayList<Weapon> getWeapons() {
        return weapons;
    }
    
    public void mountShieldBooster(int i) {
        ShieldBooster s=null;
        if(hangar!=null) {
            s=hangar.removeShieldBoosters(i);
            if(s!=null)
                shieldBoosters.add(s);
        }
    }
    
    public void mountWeapon(int i) {
        Weapon w=null;
        if(hangar!=null) {
            w=hangar.removeWeapon(i);
            if(w!=null)
                weapons.add(w);
        }
    }
    
    public void move(){
        fuelUnits-=getSpeed();
        if (fuelUnits<0)
            fuelUnits=0;
    }
    
    @Override
    public float protection() {
        float factor=1;
        for(ShieldBooster s:shieldBoosters)
            factor*=s.useIt();
        return shieldPower*factor;
    }
    
    public void receiveHangar(Hangar h) {
        if(hangar==null)
            hangar=h;
    }
    
    public boolean receiveShieldBooster(ShieldBooster s) {
        if(hangar!=null) {
            return hangar.addShieldBooster(s);
        } else {
            return false;
        }
    }
    
    @Override
    public ShotResult receiveShot(float shot) {
        float myProtection = protection();
        if(myProtection>=shot) {
            shieldPower-=SHIELDLOSSPERUNITSHOT*shot;
            shieldPower=shieldPower>(float)0.0?shieldPower:(float)0.0;
            return ShotResult.RESIST;
        } else {
            shieldPower=(float)0.0;
            return ShotResult.DONOTRESIST;
        }
    }
    
    public void receiveSupplies(SuppliesPackage s) {
        ammoPower+=s.getAmmoPower();
        fuelUnits+=s.getFuelUnits();
        shieldPower+=s.getShieldPower();
    }
    
    public boolean receiveWeapon(Weapon w) {
        if(hangar!=null) {
            return hangar.addWeapon(w);
        } else {
            return false;
        }
    }
    
    public Transformation setLoot(Loot loot) {
        CardDealer dealer = CardDealer.getInstance();
        
        int h = loot.getNHangars();
        if (h>0) {
            Hangar hangar = dealer.nextHangar();
            receiveHangar(hangar);
        }
        
        int elements = loot.getNSupplies();
        for (int i=0;i<elements;i++) {
            SuppliesPackage supply = dealer.nextSuppliesPackage();
            receiveSupplies(supply);
        }
        
        elements = loot.getNWeapons();
        for (int i=0;i<elements;i++) {
            Weapon weapon = dealer.nextWeapon();
            receiveWeapon(weapon);
        }
        
        elements = loot.getNShields();
        for (int i=0;i<elements;i++) {
            ShieldBooster shield = dealer.nextShieldBooster();
            receiveShieldBooster(shield);
        }
        
        int medals = loot.getNMedals();
        nMedals += medals;
        
        if (loot.getEfficient())
            return Transformation.GETEFFICIENT;
        else if(loot.spaceCity())
            return Transformation.SPACECITY;
        else
            return Transformation.NOTRANSFORM;
    }

    
    public void setPendingDamage(Damage d) {
        pendingDamage = d.adjust(weapons, shieldBoosters);
    }
    
    public boolean validState() {
        if(pendingDamage!=null) {
            return pendingDamage.hasNoEffect();
        } else {
            return true;
        }
    }
    
    private void assignFuelValue(float f) {
        if(f<=MAXFUEL) {
            fuelUnits=f;
        }
    }
    
    private void cleanPendingDamage() {
        if(pendingDamage==null || pendingDamage.hasNoEffect())
            pendingDamage=null;
    }
    
    public String getType() {
        return type;
    }
}
