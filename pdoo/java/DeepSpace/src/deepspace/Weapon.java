/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

/**
* @brief Armas de las que puede disponer una estación espacial para potenciar su energía al disparar
*/
class Weapon implements CombatElement{
    private final String name; ///< Nombre del arma
    private final WeaponType type; ///< Tipo de arma
    private int uses; ///< Usos restantes del arma
	
    /**
     * @brief Constructor por parámetros
     * @param _name valor de name
     * @param _type valor de type
     * @param _uses valor de uses
     */
    Weapon(String _name, WeaponType _type, int _uses) {
        name=_name;
        type=_type;
        uses=_uses;
    }
	
    /**
     * @brief Constructor por copia
     * @param s objeto a copiar
     */
    Weapon(Weapon s) {
        name=s.name;
        type=s.type;
        uses=s.uses;
    }
	
    /**
     * @brief Observador de type
     * @return type
     */
    public WeaponType getType() {return type;}
	
    /**
     * @brief Observador de uses
     * @return uses
     */
    @Override
    public int getUses() {return uses;}
	
    /**
     * @brief Observador de la potencia de disparo del arma
     * @return Potencia del tipo de arma
     */
    public float power() {return type.getPower();}
	
    /**
     * @brief Si uses es mayor que 0 lo decrementa en 1
     * @return @retval uses si uses es mayor 0
     *	   @retval 1 si uses no es mayor que 0
     */
    @Override
    public float useIt() {
        if (uses>0) {
            uses--;
            return uses;
	} else {
            return 1;
	}
    }
    
    WeaponToUI getUIversion() {
        return new WeaponToUI(this);
    }
}
