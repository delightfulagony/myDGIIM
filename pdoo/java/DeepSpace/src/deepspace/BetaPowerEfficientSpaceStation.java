/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

/**
 *
 * @author agony
 */
public class BetaPowerEfficientSpaceStation extends PowerEfficientSpaceStation{
    private static final float EXTRAEFFICIENCY=(float)1.12;
    
    BetaPowerEfficientSpaceStation(SpaceStation station) {
        super(station);
        type = "BetaPowerEfficientSpaceStation";
    }

    @Override
    protected void TextMessage() {
        System.out.println("Has ganado y te has transformado en una BetaPowerEfficientSpaceStation!");
    }
    
    @Override
    public float fire() {
        Dice dice = new Dice();
        if (dice.extraEfficiency())
            return super.fire()*EXTRAEFFICIENCY;
        else
            return super.fire();
    }

}
