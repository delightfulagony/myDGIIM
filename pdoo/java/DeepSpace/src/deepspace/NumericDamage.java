/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

import java.util.ArrayList;

/**
 *
 * @author agony
 */
public class NumericDamage extends Damage {
    private int nWeapons;
    
    NumericDamage(int w, int s) {
        super(s);
        nWeapons = w;
    }
    
    @Override
    public NumericDamageToUI getUIversion() {
        return new NumericDamageToUI(this);
    }
    
    @Override
    public NumericDamage copy() {
        return new NumericDamage(nWeapons,getNShields());
    }
    
    @Override
    public void discardWeapon(Weapon w) {
        if (nWeapons>0)
            nWeapons--;
    }

    @Override
    public NumericDamage adjust(ArrayList<Weapon> w,ArrayList<ShieldBooster> s) {
        int newShields=(getNShields()<s.size())?getNShields():s.size();
        int newWeapons=(nWeapons<w.size())?nWeapons:w.size();
        return new NumericDamage(newWeapons,newShields);
    }

    @Override
    public boolean hasNoEffect() {
        return (super.hasNoEffect() && nWeapons==0);
    }

    @Override
    public String toString() {
        String mensaje = super.toString();
        mensaje += "Se eliminan "+nWeapons+" armas.\n";
        return mensaje;
    }
    
    public int getNWeapons() {
        return nWeapons;
    }
}
