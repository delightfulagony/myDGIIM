/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;
import java.util.Random;
/**
 * @class Dice
 * @brief Esta clase tiene la responsabilidad de tomar todas las decisiones 
 * que dependen del azar en el juego.Es como una especie de dado, pero algo 
 * más sofisticado, ya que no proporciona simplemente unnúmero del 1 al 6, 
 * sino decisiones concretas en base a una serie de probabilidades establecidas. 
 * @author agony
 */
class Dice {
    private final float NHANGARSPROB;
    private final float NSHIELDSPROB;
    private final float NWEAPONSPROB;
    private final float FIRSTSHOTPROB;
    private final float EXTRAEFFICIENCYPROB;
    private final Random generator;
	
    Dice() {
        NHANGARSPROB=(float) 0.25;
        NSHIELDSPROB=(float) 0.25;
        NWEAPONSPROB=(float) 0.33;
        FIRSTSHOTPROB=(float) 0.5;
        EXTRAEFFICIENCYPROB=(float) 0.8;
        generator = new Random();
    }
        int initWithNHangar() {
    	    if (generator.nextFloat()<NHANGARSPROB)
                return 0;
            else
                return 1;
	}
	int initWithNShields() {
            if (generator.nextFloat()<NSHIELDSPROB)
                return 0;
            else
                return 1;
	}
	int initWithNWeapons() {
            if (generator.nextFloat()<NWEAPONSPROB)
                return 1;
            else if (generator.nextFloat()<NWEAPONSPROB)
                return 2;
            else
		return 3;
	}
        public int initWithNHangars(){
        if (generator.nextFloat()<NHANGARSPROB)
            return 0;
        else
            return 1;
        }
	int whoStarts(int nPlayers) {
            if (nPlayers>0)
                return generator.nextInt(nPlayers);
            else
                return 0;
	}

	GameCharacter firstShot() {
            if (generator.nextFloat()<FIRSTSHOTPROB)
                return GameCharacter.SPACESTATION;
            else
                return GameCharacter.ENEMYSTARSHIP;
	}
	    
	boolean spaceStationMoves(float speed) {
            return generator.nextFloat()<speed;
	}
        
        public boolean extraEfficiency() {
            return generator.nextFloat()<EXTRAEFFICIENCYPROB;
        }
    }
