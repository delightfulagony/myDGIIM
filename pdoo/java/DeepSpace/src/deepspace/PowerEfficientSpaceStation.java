/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

/**
 *
 * @author agony
 */
public class PowerEfficientSpaceStation extends SpaceStation{
    private static final float EFFICIENCYFACTOR = 1.10f;
    
    PowerEfficientSpaceStation(SpaceStation station) {
        super(station);
        TextMessage();
        type = "PowerEfficientSpaceStation";
    }
    
    protected void TextMessage() {
        System.out.println("Has ganado y te has transformado en una PowerEfficientSpaceStation!");
    }
    
    @Override
    public float fire() {
        return (super.fire()*EFFICIENCYFACTOR);
    }
    
    @Override
    public float protection() {
        return (super.protection()*EFFICIENCYFACTOR);
    }
    
    @Override
    public Transformation setLoot(Loot loot) {
        super.setLoot(loot);
        if (loot.getEfficient())
            return Transformation.GETEFFICIENT;
        else
            return Transformation.NOTRANSFORM;
    }

    @Override
    public String toString() {
        String aux = "#====NAVE EFICIENTE====#\n";
        aux += super.toString();
        return aux;
    }

}
