/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

import java.util.ArrayList;

/**
 *
 * @author agony
 */
public class GameUniverse {

    private static final int WIN = 10;
    
    private int currentStationIndex;
    private SpaceStation currentStation;
    private EnemyStarShip currentEnemy; 
    private ArrayList<SpaceStation> spaceStations;
    private int turns;
    private Dice dice = new Dice();
    private GameStateController gameState = new GameStateController();
    private CardDealer dealer;
    
    private boolean haveSpaceCity;
    
    public GameUniverse() {
        currentStationIndex = 0;
        currentStation = null;
        currentEnemy = null;
        spaceStations = new ArrayList<SpaceStation>();
        turns = 0;
        haveSpaceCity = false;
    }
    
    public GameUniverseToUI getUIversion() {
        return new GameUniverseToUI(currentStation, currentEnemy);
    }

    public CombatResult combat(){
        GameState state = getState();   
        if (state == GameState.BEFORECOMBAT || state == GameState.INIT)
            return combat(currentStation, currentEnemy);
        else
            return CombatResult.NOCOMBAT;
    }
    
    CombatResult combat(SpaceStation station,EnemyStarShip enemy) {
        GameCharacter ch = dice.firstShot();
        boolean enemy_wins;
        float fire;
        ShotResult result;
        CombatResult combat_result;
        
        if (ch==GameCharacter.ENEMYSTARSHIP) {
            fire = enemy.fire();
            result = station.receiveShot(fire);
            
            if (result==ShotResult.RESIST) {
                fire = station.fire();
                result = enemy.receiveShot(fire);
                enemy_wins = (result==ShotResult.RESIST);
            } else {
                enemy_wins = true;
            }
        } else {
            fire = station.fire();
            result = enemy.receiveShot(fire);
            enemy_wins = (result==ShotResult.RESIST);
        }
               
        if (enemy_wins) {
            float s = station.getSpeed();
            boolean moves = dice.spaceStationMoves(s);
            if (!moves) {
                Damage damage = enemy.getDamage();
                station.setPendingDamage(damage);
                combat_result = CombatResult.ENEMYWINS;
            } else {
                station.move();
                combat_result = CombatResult.STATIONESCAPES;
            }
        } else {
            Loot aLoot = enemy.getLoot();
            Transformation transform = station.setLoot(aLoot);
            if (transform==Transformation.GETEFFICIENT) {
                makeStationEfficient();
                combat_result = CombatResult.STATIONWINSANDCONVERTS;
            } else if (transform==Transformation.SPACECITY) {
                createSpaceCity();
                combat_result = CombatResult.STATIONWINSANDCONVERTS;
            } else {
                combat_result = CombatResult.STATIONWINS;
            }
        }
        gameState.next(turns,spaceStations.size());
        return combat_result;
    }

    
    public void discardHangar(){
        if (getState() == GameState.INIT || getState() == GameState.AFTERCOMBAT){
            spaceStations.get(currentStationIndex).discardHangar();
        }
    }
    
    public void discardWeapon(int i){
        if (getState() == GameState.INIT || getState() == GameState.AFTERCOMBAT){
            spaceStations.get(currentStationIndex).discardWeapon(i);
        }
    }
    
    public void discardWeaponInHangar(int i){
        if (getState() == GameState.INIT || getState() == GameState.AFTERCOMBAT){
            spaceStations.get(currentStationIndex).discardWeaponInHangar(i);
        }
    }
    
    public void discardShieldBooster(int i){
        if (getState() == GameState.INIT || getState() == GameState.AFTERCOMBAT){
            spaceStations.get(currentStationIndex).discardShieldBooster(i);
        }   
    }
    
    public void discardShieldBoosterInHangar(int i){
        if (getState() == GameState.INIT || getState() == GameState.AFTERCOMBAT){
            spaceStations.get(currentStationIndex).discardShieldBoosterInHangar(i);
        }
    }
    
    public GameState getState() {
        return gameState.getState();
    }

    public boolean haveAWinner() {
        return (spaceStations.get(currentStationIndex).getNMedals() >= WIN);
    }
    
    public void init(ArrayList<String> names){
        if (getState() == GameState.CANNOTPLAY){
            spaceStations = new ArrayList<>();
            dealer = CardDealer.getInstance();
            
            for(String name: names){
                SuppliesPackage supplies = dealer.nextSuppliesPackage();
                SpaceStation station = new SpaceStation(name,supplies);
                int nh = dice.initWithNHangars();
                int nw = dice.initWithNWeapons();
                int ns = dice.initWithNShields();
                
                Loot l = new Loot(0,nw,ns,nh,0);
                
                station.setLoot(l);
                spaceStations.add(station);
            }
            
            currentStationIndex = dice.whoStarts(names.size());
            currentStation = spaceStations.get(currentStationIndex);
            currentEnemy = dealer.nextEnemy();
            gameState.next(turns, spaceStations.size());
        }
    }
    
    public void mountShieldBooster(int i){
        if (getState() == GameState.INIT || getState() == GameState.AFTERCOMBAT){
            spaceStations.get(currentStationIndex).mountShieldBooster(i);
        }
    }
    
    public void mountWeapon(int i){
        if (getState() == GameState.INIT || getState() == GameState.AFTERCOMBAT){
            spaceStations.get(currentStationIndex).mountWeapon(i);
        }
    }
    
    public boolean nextTurn(){
        if (getState() == GameState.AFTERCOMBAT){
            boolean stationState = currentStation.validState();
            
            if (stationState){
                currentStationIndex = (currentStationIndex + 1) % spaceStations.size();
                turns += 1;
                currentStation = spaceStations.get(currentStationIndex);
                currentStation.cleanUpMountedItems();
                dealer = CardDealer.getInstance();
                currentEnemy = dealer.nextEnemy();
                gameState.next(turns, spaceStations.size());
                return true;
            }
        }
        return false;
    }
    
    private void createSpaceCity(){
        if (!haveSpaceCity){
            ArrayList<SpaceStation> ships = new ArrayList(spaceStations);
            ships.remove(currentStationIndex);
            currentStation = new SpaceCity(currentStation, ships);
            spaceStations.set(currentStationIndex, currentStation);
            haveSpaceCity = true;
        }
    }
    
    private void makeStationEfficient(){
        boolean extra = dice.extraEfficiency();
        if (extra){
            currentStation = new BetaPowerEfficientSpaceStation(currentStation);
        }else{
            currentStation = new PowerEfficientSpaceStation(currentStation);
        }
        
        spaceStations.set(currentStationIndex, currentStation);
}
}
