/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

class SuppliesPackage {
    private final float ammoPower; ///< Potencia de la munición
    private final float fuelUnits; ///< Unidades de combustible
    private final float shieldPower; ///< Potencia del escudo
    
    /**
     * @brief Constructor por parámetros
     * @param _ammoPower valor de ammoPower
     * @param _fuelUnits valor de fuelUnits
     * @param _shieldPower valor de shieldPower
     */
    SuppliesPackage(float _ammoPower, float _fuelUnits, float _shieldPower) {
        ammoPower=_ammoPower;
        fuelUnits=_fuelUnits;
        shieldPower=_shieldPower;
    }
	
    /**
     * @brief Constructor por copia
     * @param s objeto a copiar
     */
    SuppliesPackage(SuppliesPackage s) {
        ammoPower=s.ammoPower;
        fuelUnits=s.fuelUnits;
        shieldPower=s.shieldPower;
    }
    
    /**
     * @brief Observador de ammoPower
     * @return ammoPower
     */
    float getAmmoPower() {return ammoPower;}
    
    /**
     * @brief Observador de fuelUnits
     * @return fuelUnits
     */
    float getFuelUnits() {return fuelUnits;}
	
    /**
     * @brief Observador de shieldPower
     * @return shieldPower
     */
    float getShieldPower() {return shieldPower;}
}
