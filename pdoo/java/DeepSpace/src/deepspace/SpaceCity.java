/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

import java.util.ArrayList;

/**
 *
 * @author agony
 */
public class SpaceCity extends SpaceStation{
    private SpaceStation base;
    private ArrayList<SpaceStation> collaborators;
    
    public SpaceCity(SpaceStation base, ArrayList<SpaceStation> rest) {
        super(base);
        this.base = base;
        this.collaborators = rest;
        System.out.println("Has ganado y te has transformado en una SpaceCity!");
        type = "SpaceCity";
    }
    
    public ArrayList<SpaceStation> getCollaborators() {
        return collaborators;
    }
    
    @Override
    public float fire() {
        float fire_power = super.fire();
        for (SpaceStation s: collaborators)
            fire_power += s.fire();
        return fire_power;
    }

    
    @Override
    public float protection() {
        float shield_power = super.protection();
        for (SpaceStation s: collaborators)
            shield_power += s.protection();
        return shield_power;
    }

    
    @Override
    public Transformation setLoot(Loot loot) {
        super.setLoot(loot);
        return Transformation.NOTRANSFORM;
    }

    @Override
    public String toString() {
        String aux = "#====CIUDAD ESPACIAL====#\nCiudad base:\n";
        aux += super.toString() + "\nColaboradores:\n";
        for (SpaceStation s: collaborators)
            aux += s.toString();
        return aux;
    }

}
