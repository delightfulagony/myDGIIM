/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

/**
 * @brief Representación del escudo de una estación espacial
 */
class ShieldBooster implements CombatElement{
    private final String name; ///< Nombre del escudo
    private final float boost; ///< Potenciador de escudo
    private int uses; ///< Usos restantes del escudo
	
    /**
     * @brief Constructor por parámetros
     * @param _name valor de name
     * @param _boost valor de boost
     * @param _uses valor de uses
     */
    ShieldBooster(String _name, float _boost, int _uses) {
        name=_name;
        boost=_boost;
        uses=_uses;
    }
	
    /**
     * @brief Constructor por copia
     * @param s objeto a copiar
     */
    ShieldBooster(ShieldBooster s) {
        name=s.name;
        boost=s.boost;
        uses=s.uses;
    }
	
    /**
     * @brief Observador de name
     * @return name
     */
    public String getName() {return name;}
	
    /**
     * @brief Observador de boost
     * @return boost
     */
    public float getBoost() {return boost;}
	
    /**
     * @brief Observador de uses
     * @return uses
     */
    @Override
    public int getUses() {return uses;}
	
    /**
     * @brief Si uses es mayor que 0 lo decrementa en 1
     * @return @retval uses si uses es mayor 0
     *	   @retval 1 si uses no es mayor que 0
     */
    @Override
    public float useIt() {
	if (uses>0) {
            uses--;
            return uses;
        } else {
            return 1;
        }
    }
    
    ShieldToUI getUIversion() {
        return new ShieldToUI(this);
    }
}
