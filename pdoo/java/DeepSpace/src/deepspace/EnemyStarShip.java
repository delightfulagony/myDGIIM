/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

/**
 *
 * @author agony
 */
public class EnemyStarShip implements SpaceFighter{
    float ammoPower;
    String name;
    float shieldPower;
    Loot loot;
    Damage damage;
    
    protected EnemyStarShip(String n, float a, float s, Loot l, Damage d) {
        name = n;
        ammoPower = a;
        shieldPower = s;
        loot = l;
        damage = d;
    }
    
    protected EnemyStarShip(EnemyStarShip e) {
        name = e.name;
        ammoPower = e.ammoPower;
        shieldPower = e.shieldPower;
        loot = e.loot;
        damage = e.damage;
    }
    
    protected EnemyToUI getUIversion() {
        return new EnemyToUI(this);
    }
    
    @Override
    public float fire() {
        return ammoPower;
    }
    
    public float getAmmoPower() {
        return ammoPower;
    }
    
    public Damage getDamage() {
        return damage;
    }
    
    public Loot getLoot() {
        return loot;
    }
    
    public String getName() {
        return name;
    }
    
    public float getShieldPower() {
        return shieldPower;
    }
    
    @Override
    public float protection() {
        return shieldPower;
    }
    
    public ShotResult receiveShot(float shot) {
        return shot>shieldPower?(ShotResult.DONOTRESIST):(ShotResult.RESIST);
    }
    
    public EnemyStarShip copy() {
       return new EnemyStarShip(this);
    }
    
    @Override
    public String toString() {
        String aux = "Nombre del enemigo: "+name;
        aux += "\nPoder de disparo: "+ammoPower;
        aux += "\nPoder de escudo: "+shieldPower;
        aux += "\nDaño de la nave:\n"+damage.toString();
        aux += "\nBotín de la nave:\n"+loot.toString();
        return aux;
    }
}
