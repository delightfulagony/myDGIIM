/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

import java.util.ArrayList;

/**
 *
 * @author agony
 */
abstract class Damage {
    private int nShields;
    
    Damage (int s) {
        nShields = s;
    }
    
    public abstract DamageToUI getUIversion();
    public abstract Damage copy();
    public abstract void discardWeapon(Weapon w);
    public abstract Damage adjust(ArrayList<Weapon> w, ArrayList<ShieldBooster> s);

    public void discardShieldBooster() {
        if (nShields>0)
            nShields--;
    }
    
    public boolean hasNoEffect() {
        return nShields==0;
    }
    
    public int getNShields() {
        return nShields;
    }

    @Override
    public String toString(){
        String mensaje="Se eliminan: "+nShields+" escudos.\n";
        return mensaje;
    }
}