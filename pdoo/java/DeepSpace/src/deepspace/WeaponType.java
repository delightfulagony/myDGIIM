/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

/**
* @brief Posibles tipos de armas
*/
public enum WeaponType {
    LASER(2),
    MISSILE(3),
    PLASMA(4);
	
    private final float power; ///<Potencia del tipo de arma
	
    /**
     * @brief Constructor por parámetros del objeto WeaponType
     * @param _power Potencia del tipo de arma
     */
    WeaponType(float _power) {
        power=_power;
    }
	
    /**
     * @brief Observador del poder del tipo de arma
     * @return Poder del tipo de arma
     */
    float getPower() {
        return power;
    }
    /*
    @Override
    public String toString() {
        if (power==2)
            return "LASER";
        else if (power==3)
            return "MISSILE";
        else
            return "PLASMA";
    }*/
}