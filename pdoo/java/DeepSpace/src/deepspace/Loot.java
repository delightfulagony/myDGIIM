/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

/**
* @brief Botín que se puede obtener de un enemigo
*/
class Loot {
    private int nSupplies; ///< Suministros
    private int nWeapons; ///< Armas
    private int nShields; ///< Escudos
    private int nHangars; ///< Hangares
    private int nMedals; ///< Medallas
    private boolean getEfficient;
    private boolean spaceCity;

    Loot(int _nSupplies, int _nWeapons, int _nShields, int _nHangars, int _nMedals, boolean ef, boolean city) {
        nSupplies=_nSupplies;
        nWeapons=_nWeapons;
        nShields=_nShields;
        nHangars=_nHangars;
        nMedals=_nMedals;
        getEfficient=ef;
        spaceCity=city;
    }
        /**
     * @brief Constructor por parámetros
     * @param _nSupplies valor de nSupplies
     * @param _nWeapons valor de nWeapons
     * @param _nShields valor de nShields
     * @param _nHangars valor de nHangars
     * @param _nMedals valor de nMedals
     */
    Loot(int _nSupplies, int _nWeapons, int _nShields, int _nHangars, int _nMedals) {
        nSupplies=_nSupplies;
        nWeapons=_nWeapons;
        nShields=_nShields;
        nHangars=_nHangars;
        nMedals=_nMedals;
    }
    /**
     * @brief Observador de nSupplies
     * @return nSupplies
     */
    int getNSupplies() {return nSupplies;}
    /**
     * @brief Observador de nWeapons
     * @return nWeapons
     */
    int getNWeapons() {return nWeapons;}
    /**
     * @brief Observador de nShields
     * @return nShields
     */
    int getNShields() {return nShields;}
    /**
     * @brief Observador de nHangars
     * @return nHangars
     */
    int getNHangars() {return nHangars;}
    /**
     * @brief Observador de nMedals
     * @return nMedals
     */
    int getNMedals() {return nMedals;}
    
    public boolean getEfficient() { return getEfficient; }
    
    public boolean spaceCity() { return spaceCity; }
    /**
     * @brief Interfaz de Loot
     * @return Objeto LootToUI de la instancia
     */
    LootToUI getUIversion() {return new LootToUI(this);}
    
    @Override
    public String toString() {
        String aux = "Suministros: " + nSupplies;
        aux += ", Armas: " + nWeapons;
        aux += ", Escudos: " + nShields;
        aux += ", Hangares: " + nHangars;
        aux += ", Medallas: " + nMedals + "\n";
        return aux;
    }
}
