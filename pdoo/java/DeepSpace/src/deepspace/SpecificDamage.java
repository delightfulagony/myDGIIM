/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

import java.util.ArrayList;

/**
 *
 * @author agony
 */
public class SpecificDamage extends Damage {
    private ArrayList<WeaponType> weapons;
    
    SpecificDamage(ArrayList<WeaponType> w, int s) {
        super(s);
        weapons = w;
    }
    
    @Override
    public SpecificDamageToUI getUIversion() {
        return new SpecificDamageToUI(this);
    }
    
    @Override
    public SpecificDamage copy() {
        return new SpecificDamage(weapons, getNShields());
    }
    
    @Override
    public void discardWeapon(Weapon w) {
        for(WeaponType wi:weapons) {
            if (w.getType()==wi) {
                weapons.remove(wi);
                break;
            }
        }
    }

    @Override
    public SpecificDamage adjust(ArrayList<Weapon> w, ArrayList<ShieldBooster> s) {
        int newShields = (getNShields()<s.size())?getNShields():s.size();
        ArrayList<WeaponType> newWeapons = new ArrayList<>();
        
        for (int i=0;i<weapons.size();i++) {
            int index = arrayContainsType(w,weapons.get(i));
            if (index!=-1) {
                int tipoEnDanio = 0, tipoEnArmas = 1;
                for (WeaponType wt: newWeapons)
                    if (wt==weapons.get(i))
                        tipoEnDanio++;
                for (int j=index+1;j<w.size();j++)
                    if (w.get(j).getType()==weapons.get(i))
                        tipoEnArmas++;
                if (tipoEnDanio<tipoEnArmas)
                    newWeapons.add(weapons.get(i));
            }
        }
        return new SpecificDamage(newWeapons,newShields);
    }

    @Override
    public boolean hasNoEffect() {
        return (super.hasNoEffect() && weapons.isEmpty());
    }
    
    @Override
    public String toString() {
        String mensaje = super.toString();
        mensaje += "Armas eliminadas: ";
        for (WeaponType w: weapons) {
            mensaje += w.toString() + " ";
        }
        return (mensaje+"\n");
    }
    
    public ArrayList<WeaponType> getWeapons() {
        return weapons;
    }
    
    private int arrayContainsType(ArrayList<Weapon> w,WeaponType t) {
        for (int i=0;i<w.size();i++) {
            if (w.get(i).getType()==t)
                return i;
        }
        return -1;
    }
}
