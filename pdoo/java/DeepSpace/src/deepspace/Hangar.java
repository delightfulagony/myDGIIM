/*
 * Copyright (C) 2019 agony
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package deepspace;

import java.util.ArrayList;

public class Hangar {
    private final int maxElements;
    ArrayList<Weapon> weapons;
    ArrayList<ShieldBooster> shieldBoosters;
    
    protected Hangar(int capacity) {
        maxElements = capacity;
        weapons = new ArrayList<>();
        shieldBoosters = new ArrayList<>();
    }
    
    protected Hangar(Hangar h) {
        maxElements = h.maxElements;
        weapons = h.weapons;
        shieldBoosters = h.shieldBoosters;
    }
    
    protected HangarToUI getUIversion() {
        return new HangarToUI(this);
    }
    
    private boolean spaceAvailable() {
        return (weapons.size()+shieldBoosters.size())<maxElements;
    }
    
    public boolean addWeapon(Weapon w) {
        if (spaceAvailable()) {
            weapons.add(w);
            return true;
        }
        return false;
    }
    
    public boolean addShieldBooster(ShieldBooster s) {
        if (spaceAvailable()) {
            shieldBoosters.add(s);
            return true;
        }
        return false;
    }
    
    public int getMaxElements() {
        return maxElements;
    }
    
    public ArrayList<ShieldBooster> getShieldBoosters() {
        return shieldBoosters;
    }
    
    public ArrayList<Weapon> getWeapons() {
        return weapons;
    }
    
    public ShieldBooster removeShieldBoosters(int s) {
        if (shieldBoosters!=null) {
            if (s>=0)
                return shieldBoosters.remove(s);
        }
        return null;
    }
    
    public Weapon removeWeapon(int w) {
        if (weapons!=null) {
            if (w>=0)
                return weapons.remove(w);
        }
        return null;
    }
}
