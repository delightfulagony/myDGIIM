/*
 * @author: delightfulagony
 */

#include <iostream>
#include <set>

using namespace std;

// Funciones auxiliares
void showset(const set<int> &s);
void infoset(const set<int> &s);
set<int> unionset(const set<int> &A, const set<int> &B);
set<int> intersectionset(const set<int> &A, const set<int> &B);
set<int> complementoset(const set<int> &A, const set<int> &B);

void full();

set<int> grupo1;
set<int> grupo2;

// Programa principal
int main(int argc, char * argv[]) {
	
	unsigned int num;
	int aux;
	cout<<"Cuantos enteros tendrá el primer grupo? \n";
	cin>>num;
	for (size_t i=0;i<num;i++) {
		cin>>aux;
		grupo1.insert(aux);
	}

	cout<<"Cuantos enteros tendrá el segundo grupo? \n";
	cin>>num;
	for (size_t i=0;i<num;i++) {
		cin>>aux;
		grupo2.insert(aux);
	}

	full();
	cout<<"Probamos a swapear los conjuntos\n";
	grupo1.swap(grupo2);
	full();

}

// Funciones auxiliares
void showset(const set<int> &s) {
	for(set<int>::iterator i=s.begin() ; i!=s.end() ; i++) {
		cout << *i << " ";
	}
	cout << endl;
}

void infoset(const set<int> &s) {
	cout<<"Numero de elementos "<<s.size()<<endl;
	showset(s);
}

set<int> unionset(const set<int> &A, const set<int> &B) {
	set<int> unio;
	for(set<int>::iterator a=A.begin() ; a!=A.end() ; a++) 
		unio.insert(*a);
	for(set<int>::iterator b=B.begin() ; b!=B.end() ; b++)
		unio.insert(*b);

	return unio;
}

set<int> intersectionset(const set<int> &A, const set<int> &B) {
	set<int> inter;
	for(set<int>::iterator a=A.begin() ; a!=A.end() ; a++)
		if (B.find(*a)!=B.end()) inter.insert(*a);
	
	return inter;
}

set<int> complementoset(const set<int> &A, const set<int> &B) {
	set<int> comp;
	for(set<int>::iterator a=A.begin() ; a!=A.end() ; a++) {
		if (B.find(*a)==B.end())
			comp.insert(*a);
	}
	return comp;
}

void full() {
	set<int> setunion = unionset(grupo1, grupo2);
	set<int> setintersection = intersectionset(grupo1, grupo2);
	set<int> setcomplemento = complementoset(grupo1, grupo2);

	cout<<"Grupo 1: \n";
	infoset(grupo1);
	cout<<"Grupo 2: \n";
	infoset(grupo2);
	cout<<"Union: \n";
	infoset(setunion);
	cout<<"Interseccion: \n";
	infoset(setintersection);
	cout<<"Complemento: \n";
	infoset(setcomplemento);
}
