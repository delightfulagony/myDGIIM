#include <iostream>
#include <set>

using namespace std;

int main() {
	multiset<int, less<int>> ms;
	ms.insert(10);
	ms.insert(35);
	ms.insert(10);

	cout<<"\nHay "<<ms.count(10)<<" elementos igual a 10."<<endl;

	multiset<int, less<int>>::iterator it;

	it=ms.find(10);

	if (it !=ms.end()) cout<<"\nSe ha encontrado un 10."<<endl;

	return 0;
}
