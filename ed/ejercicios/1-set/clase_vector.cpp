#include <iostream>
#include <vector>

using namespace std;

int main() {
	vector<int> v;

	//Añadimos elementos al final del vector por medio de vector::push_back();
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	v.push_back(9);

	//Mostramos el tamaño y capacidad del vector
	cout<<"\nTamaño del vector: "<<v.size()
		<<"\nCapacidad del vector: "<<v.capacity();

	//Definimos un iterador para vector<int>
	vector<int>::const_iterator it;

	//y usamos el iterador para recorrer el bucle
	for (it=v.begin();it!=v.end();++it)
		cout<<*it<<endl;

	return 0;
}
