#include <iostream>
#include <list>

using namespace std;

int main() {
	list<int> l;

	//Añadimos elementos al final del vector por medio de vector::push_back();
	l.push_back(2);
	l.push_back(3);
	l.push_back(4);
	l.push_back(5);
	l.push_back(6);
	l.push_back(7);
	l.push_back(8);
	l.push_back(9);

	//Mostramos el tamaño y capacidad del vector
	cout<<"\nTamaño de la lista: "<<l.size();

	//Definimos un iterador para vector<int>
	list<int>::const_iterator it;

	//y usamos el iterador para recorrer el bucle
	for (it=l.begin();it!=l.end();++it)
		cout<<*it<<endl;

	return 0;
}
