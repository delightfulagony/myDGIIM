#include <iostream>
#include <deque>

using namespace std;

int main() {
	deque<int> d;

	//Añadimos elementos al final del vector por medio de vector::push_back();
	d.push_back(2);
	d.push_back(3);
	d.push_back(4);
	d.push_back(5);
	d.push_back(6);
	d.push_back(7);
	d.push_back(8);
	d.push_back(9);

	//Mostramos el tamaño y capacidad del vector
	cout<<"\nTamaño del deque : "<<d.size();

	//Definimos un iterador para vector<int>
	deque<int>::const_iterator it;

	//y usamos el iterador para recorrer el bucle
	for (it=d.begin();it!=d.end();++it)
		cout<<*it<<endl;

	return 0;
}
